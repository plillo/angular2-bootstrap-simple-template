**Per servire la distribuzione in locale**
http-server dist/template-app

**Per riformattare**
SHIFT+ALT+F

**Per eseguire una build ottimizzata per la produzione**
(ng build --prod --aot --vendor-chunk --common-chunk --delete-output-path --buildOptimizer)
npm run ng build --prod --aot --vendor-chunk --common-chunk --delete-output-path --buildOptimizer

