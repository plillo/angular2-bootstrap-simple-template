import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TimepickerComponent } from 'ngx-bootstrap/timepicker';
import { CanActivateViaAuthGuard } from './classes/canactivate';
import { AdministrationComponent } from './components/administration/administration.component';
import { BackendConfigComponent } from './components/backend-config/backend-config.component';
import { ButtonsComponent } from './components/demos/buttons/buttons.component';
import { DatesComponent } from './components/demos/dates/dates.component';
import { DemosComponent } from './components/demos/demos.component';
import { DropdownComponent } from './components/demos/dropdown/dropdown.component';
import { LandingPageComponent } from './components/landing-page/landing-page.component';
import { ListExampleComponent } from './components/list-example/list-example.component';
import { MeteoEditComponent } from './components/meteo-edit/meteo-edit.component';
import { MeteoListComponent } from './components/meteo-list/meteo-list.component';
import { PixiExampleComponent } from './components/pixi-example/pixi-example.component';
import { PixiComponent } from './components/pixi/pixi.component';
// BASE imports
import { WhoWeAreComponent } from './components/who-we-are/who-we-are.component';
import { ProfileComponent } from './base/components/profile/profile.component';
import { SettingsComponent } from './base/components/settings/settings.component';
import { SignupComponent } from './base/components/signup/signup.component';

const routes: Routes = [
  { path: '', redirectTo: 'landing', pathMatch: 'full' },
  { path: 'home', component: LandingPageComponent },
  { path: 'link2', component: PixiComponent },
  { path: 'link3', component: PixiExampleComponent },
  { path: 'link4', component: ListExampleComponent },
  { path: 'meteo-edit', component: MeteoEditComponent },
  { path: 'meteo-list', component: MeteoListComponent },
  { path: 'landing', component: LandingPageComponent },
  { path: 'administration', component: AdministrationComponent, canActivate:  [CanActivateViaAuthGuard],
    children: [
      { path: 'backend', component: BackendConfigComponent }
    ]
  },
  { path: 'whoweare', component: WhoWeAreComponent },
  { path: 'demo', component: DemosComponent,
    children: [
      { path: 'dates', component: DatesComponent },
      { path: 'timepicker', component: TimepickerComponent },
      { path: 'dropdown', component: DropdownComponent },
      { path: 'buttons', component: ButtonsComponent },
    ] 
  },
  { path: 'signup', component: SignupComponent },
  { path: 'profile', component: ProfileComponent },
  { path: 'settings', component: SettingsComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes,{
    onSameUrlNavigation: 'reload',
    useHash: true
  })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
