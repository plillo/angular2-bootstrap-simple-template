import { Directive, ElementRef, Input } from '@angular/core';
import * as PIXI from 'pixi.js/dist/pixi.js';
import Rectangle = PIXI.Rectangle;

@Directive({
  selector: '[pixi-example]'
})
export class PixiExampleDirective {
  // static PIXI options
  protected static OPTIONS: Object = {
    //backgroundColor: 0xF0F8FF,
    transparent: true,
    antialias: true
  };

  protected _clickFcn: Function;                  // reference to function used to handle clicks in the display area
  protected _rect: Rectangle;                     // display area bounding rectangle

  // PIXI app and stage references
  protected _app: PIXI.Application;
  protected _stage: PIXI.Container;

    /**
   * Specify the display width in pixels
   *
   * @type {number} value Pixel width (must be greater than zero)
   */
  @Input('width')
  public set width(value: number)
  {
    this._width = !isNaN(value) && value > 0 ? value : this._width;
  }

  /**
   * Specify the display height in pixels
   *
   * @type {number} value Pixel height (must be greater than zero)
   */
  @Input('height')
  public set height(value: number)
  {
    this._height = !isNaN(value) && value > 0 ? value : this._height;
  }

  protected _width: number  = 800;    // display width in px
  protected _height: number = 600;    // display height in px

  constructor(protected _elRef: ElementRef) { }

    /**
   * Angular lifecycle (on init)
   *
   * @returns {nothing} Performs all PIXI setup
   */
  public ngOnInit(): void
  {
    const options = Object.assign({width: this._width, height: this._height}, PixiExampleDirective.OPTIONS);

    this._app = new PIXI.Application(options);
    this._elRef.nativeElement.appendChild(this._app.view);

    this._clickFcn = ()=>{alert('WWW');}
    this._elRef.nativeElement.addEventListener('click', this._clickFcn);

    this._rect  = this._elRef.nativeElement.getBoundingClientRect();
    this._stage = this._app.stage;

    // in case you want to see what renderer you have (otherwise comment out)
    const renderer: PIXI.WebGLRenderer | PIXI.CanvasRenderer = this._app.renderer;
    console.log( "renderer is: ", renderer );

    // remaining PIXI setup
    //this.__setup();


    // create a new Sprite from an image path.
    var bunny = PIXI.Sprite.fromImage('../../assets/images/no-picture.png');

    // center the sprite's anchor point
    bunny.anchor.set(0.5);

    // move the sprite to the center of the screen
    bunny.x = this._app.screen.width / 2;
    bunny.y = this._app.screen.height / 2;

    this._app.stage.addChild(bunny);

    this._app.ticker.add(function() {
        // just for fun, let's rotate mr rabbit a little
        bunny.rotation += 0.1;
    });
  }

  /**
   * Angular lifecycle (on destroy)
   *
   * @returns {nothing}
   */
  public ngOnDestroy(): void
  {
    /*
    if (this._elRef.nativeElement.hasEventListener('click')) {
      this._elRef.nativeElement.removeEventListener('click', this._clickFcn);
    }
    */
  }

}
