import {
  Component,
  Renderer,
  ElementRef,
  OnInit,
  ViewChild,
} from "@angular/core";
import { setTheme } from "ngx-bootstrap/utils";
import { Router, NavigationEnd } from "@angular/router";
import { Subscription } from "rxjs";
import { filter } from "rxjs/operators";
import * as $ from "jquery";
import { NavbarComponent } from "./base/components/layout/navbar/navbar.component";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"],
})
export class AppComponent implements OnInit {
  private _router: Subscription;

  @ViewChild(NavbarComponent, { static: true }) navbar: NavbarComponent;

  constructor(
    private renderer: Renderer,
    private router: Router,
    private element: ElementRef
  ) {
    setTheme("bs4"); // or 'bs3'
  }

  ngOnInit() {
    var navbar: HTMLElement = this.element.nativeElement.children[0].children[0]
      .children[1];
    this._router = this.router.events
      .pipe(filter((event) => event instanceof NavigationEnd))
      .subscribe((event: NavigationEnd) => {
        if (window.outerWidth > 991) {
          window.document.children[0].scrollTop = 0;
        } else {
          window.document.activeElement.scrollTop = 0;
        }
        this.navbar.sidebarClose();
      });
    this.renderer.listenGlobal("window", "scroll", (event) => {
      const number = window.scrollY;
      if (number > 150 || window.pageYOffset > 150) {
        // add/remove logic
        this.element.nativeElement.parentElement.classList.add("scrolled-mode");
        navbar.classList.add("navbar-low");
        navbar.classList.add("navbar-hide");
      } else {
        // add/remove logic
        this.element.nativeElement.parentElement.classList.remove(
          "scrolled-mode"
        );
        navbar.classList.remove("navbar-low");
        navbar.classList.remove("navbar-hide");
      }
    });
    var ua = window.navigator.userAgent;
    var trident = ua.indexOf("Trident/");
    if (trident > 0) {
      // IE 11 => return version number
      var rv = ua.indexOf("rv:");
      var version = parseInt(ua.substring(rv + 3, ua.indexOf(".", rv)), 10);
    }
    if (version) {
      var body = document.getElementsByTagName("body")[0];
      body.classList.add("ie-background");
    }

    let dynamicColor = this.getRandomColor();
    dynamicColor = this.increase_brightness(dynamicColor, 10);
    let complementarycolor = this.hexToComplimentary(dynamicColor);
    let brightcolor = this.increase_brightness(dynamicColor, 15);

    $("body").css("background-color", dynamicColor);
    $(".dynamic-color").css("color", dynamicColor);
    $(".dynamic-background-color").css("background-color", dynamicColor);
    $(".dynamic-border-color").css("border-color", dynamicColor);
    $(".dynamic-background-bright-color").css("background-color", brightcolor);

    let elem = $(".actual-background");
    elem.html(dynamicColor);
  }

  getRandomColor() {
    var letters = "0123456789ABCDEF".split("");
    var color = "#";
    for (var i = 0; i < 6; i++) {
      color += letters[6 + Math.floor(Math.random() * 9)];
    }

    return color;
  }

  hexToComplimentary(hex) {
    // Convert hex to rgb
    // Credit to Denis http://stackoverflow.com/a/36253499/4939630
    let rgb: any =
      "rgb(" +
      (hex = hex.replace("#", ""))
        .match(new RegExp("(.{" + hex.length / 3 + "})", "g"))
        .map(function (l) {
          return parseInt(hex.length % 2 ? l + l : l, 16);
        })
        .join(",") +
      ")";

    // Get array of RGB values
    let rgbs = rgb.replace(/[^\d,]/g, "").split(",");

    var r: any = rgbs[0],
      g: any = rgbs[1],
      b: any = rgbs[2];

    // Convert RGB to HSL
    // Adapted from answer by 0x000f http://stackoverflow.com/a/34946092/4939630
    r /= 255.0;
    g /= 255.0;
    b /= 255.0;
    var max = Math.max(r, g, b);
    var min = Math.min(r, g, b);
    var h,
      s,
      l = (max + min) / 2.0;

    if (max == min) {
      h = s = 0; //achromatic
    } else {
      var d = max - min;
      s = l > 0.5 ? d / (2.0 - max - min) : d / (max + min);

      if (max == r && g >= b) {
        h = (1.0472 * (g - b)) / d;
      } else if (max == r && g < b) {
        h = (1.0472 * (g - b)) / d + 6.2832;
      } else if (max == g) {
        h = (1.0472 * (b - r)) / d + 2.0944;
      } else if (max == b) {
        h = (1.0472 * (r - g)) / d + 4.1888;
      }
    }

    h = (h / 6.2832) * 360.0 + 0;

    // Shift hue to opposite side of wheel and convert to [0-1] value
    h += 180;
    if (h > 360) {
      h -= 360;
    }
    h /= 360;

    // Convert h s and l values into r g and b values
    // Adapted from answer by Mohsen http://stackoverflow.com/a/9493060/4939630
    if (s === 0) {
      r = g = b = l; // achromatic
    } else {
      var hue2rgb = function hue2rgb(p, q, t) {
        if (t < 0) t += 1;
        if (t > 1) t -= 1;
        if (t < 1 / 6) return p + (q - p) * 6 * t;
        if (t < 1 / 2) return q;
        if (t < 2 / 3) return p + (q - p) * (2 / 3 - t) * 6;
        return p;
      };

      var q = l < 0.5 ? l * (1 + s) : l + s - l * s;
      var p = 2 * l - q;

      r = hue2rgb(p, q, h + 1 / 3);
      g = hue2rgb(p, q, h);
      b = hue2rgb(p, q, h - 1 / 3);
    }

    r = Math.round(r * 255);
    g = Math.round(g * 255);
    b = Math.round(b * 255);

    // Convert r b and g values to hex
    rgb = b | (g << 8) | (r << 16);
    return "#" + (0x1000000 | rgb).toString(16).substring(1);
  }

  hue2rgb(p, q, t) {
    if (t < 0) t += 1;
    if (t > 1) t -= 1;
    if (t < 1 / 6) return p + (q - p) * 6 * t;
    if (t < 1 / 2) return q;
    if (t < 2 / 3) return p + (q - p) * (2 / 3 - t) * 6;
    return p;
  }

  /**
   * Converts an RGB color value to HSL. Conversion formula
   * adapted from http://en.wikipedia.org/wiki/HSL_color_space.
   * Assumes r, g, and b are contained in the set [0, 255] and
   * returns h, s, and l in the set [0, 1].
   *
   * @param   Number  r       The red color value
   * @param   Number  g       The green color value
   * @param   Number  b       The blue color value
   * @return  Array           The HSL representation
   */
  rgbToHsl(r, g, b) {
    (r /= 255), (g /= 255), (b /= 255);
    var max = Math.max(r, g, b),
      min = Math.min(r, g, b);
    var h,
      s,
      l = (max + min) / 2;

    if (max == min) {
      h = s = 0; // achromatic
    } else {
      var d = max - min;
      s = l > 0.5 ? d / (2 - max - min) : d / (max + min);
      switch (max) {
        case r:
          h = (g - b) / d + (g < b ? 6 : 0);
          break;
        case g:
          h = (b - r) / d + 2;
          break;
        case b:
          h = (r - g) / d + 4;
          break;
      }
      h /= 6;
    }

    return [h, s, l];
  }

  /**
   * Converts an HSL color value to RGB. Conversion formula
   * adapted from http://en.wikipedia.org/wiki/HSL_color_space.
   * Assumes h, s, and l are contained in the set [0, 1] and
   * returns r, g, and b in the set [0, 255].
   *
   * @param   Number  h       The hue
   * @param   Number  s       The saturation
   * @param   Number  l       The lightness
   * @return  Array           The RGB representation
   */
  hslToRgb(h, s, l) {
    var r, g, b;

    if (s == 0) {
      r = g = b = l; // achromatic
    } else {
      var q = l < 0.5 ? l * (1 + s) : l + s - l * s;
      var p = 2 * l - q;
      r = this.hue2rgb(p, q, h + 1 / 3);
      g = this.hue2rgb(p, q, h);
      b = this.hue2rgb(p, q, h - 1 / 3);
    }

    return [r * 255, g * 255, b * 255];
  }

  increase_brightness(rgbcode, percent) {
    var r = parseInt(rgbcode.slice(1, 3), 16),
      g = parseInt(rgbcode.slice(3, 5), 16),
      b = parseInt(rgbcode.slice(5, 7), 16),
      HSL = this.rgbToHsl(r, g, b),
      newBrightness = HSL[2] + HSL[2] * (percent / 100),
      RGB;

    RGB = this.hslToRgb(HSL[0], HSL[1], newBrightness);
    rgbcode =
      "#" +
      this.convertToTwoDigitHexCodeFromDecimal(RGB[0]) +
      this.convertToTwoDigitHexCodeFromDecimal(RGB[1]) +
      this.convertToTwoDigitHexCodeFromDecimal(RGB[2]);

    return rgbcode;
  }

  convertToTwoDigitHexCodeFromDecimal(decimal) {
    var code = Math.round(decimal).toString(16);

    code.length > 1 || (code = "0" + code);
    return code;
  }
}
