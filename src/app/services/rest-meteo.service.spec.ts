import { TestBed } from '@angular/core/testing';

import { RestMeteoService } from './rest-meteo.service';

describe('RestMeteoService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RestMeteoService = TestBed.get(RestMeteoService);
    expect(service).toBeTruthy();
  });
});
