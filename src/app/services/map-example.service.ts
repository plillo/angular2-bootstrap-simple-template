import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BackendService } from '../base/services/backend.service';

@Injectable({
  providedIn: 'root'
})
export class MapExampleService {

  constructor(private http: HttpClient, private backendService: BackendService) {}

  getAll() {
    return this.http.get(this.backendService.getApplicationBackend() + "/example/1.0/map-items/");
  }

}
