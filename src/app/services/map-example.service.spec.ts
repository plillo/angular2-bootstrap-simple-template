import { TestBed } from '@angular/core/testing';

import { MapExampleService } from './map-example.service';

describe('MapExampleService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MapExampleService = TestBed.get(MapExampleService);
    expect(service).toBeTruthy();
  });
});
