import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RestMeteoService {
    // MOCKED data
    // ===========
    mockedDataMeteoList: any[] = [{
        uuid: '1',
        date: this.getDate(),
        air:{
          temp: 28.7,
          humidity: 88
        },
        wind:{
          velocity: 11.7,
          degrees: 23.49
        }
      },
      {
        uuid: '2',
        date:  this.getDate(),
        air:{
          temp: 25.1,
          humidity: 81
        },
        wind:{
          velocity: 13.0,
          degrees: 26.33
        }
      },
      {
        uuid: '3',
        date:  this.getDate(),
        air:{
          temp: 24.5,
          humidity: 77.05
        },
        wind:{
          velocity: 11.9,
          degrees: 26.31
        }
      }
    ]

  constructor(private http: HttpClient) { }

  getDate() {
    let s = Math.floor(Math.random() * 3600);
    let d = new Date();
    d.setSeconds(d.getSeconds() + s);

    return d;
  }

  get(uuid: string) {
    //return this.http.get('http://localhost:3000/data'+ uuid, {});

    // return mocked data
    let i = 0;
    for(const element of this.mockedDataMeteoList) {
      if(element.uuid === uuid)
        return new Observable<any> ((observer)=> {observer.next({item:this.mockedDataMeteoList[i]})});
      else i+=1;
    }

    return new Observable<any> ((observer)=> {observer.next({item:undefined})});
  }

  getList() {
    // return this.http.get("http://localhost:3000/data");

    // return mocked data
    return new Observable<any> ((observer)=> {observer.next({count:this.mockedDataMeteoList.length, list:this.mockedDataMeteoList})});
  }

  post(data: any) {
    //return this.http.post("http://localhost:3000/data", data);

    // add mocked data
    this.mockedDataMeteoList.push(data);
    return new Observable<any> ((observer)=> {observer.next({payload:{}})});
  }

  put(uuid: string, data: any) {
    //return this.http.put('http://localhost:3000/data/' + uuid, data);

    // update mocked data
    let i = 0;
    for(const element of this.mockedDataMeteoList) {
      if(element.uuid === uuid) {
        this.mockedDataMeteoList[i] = data;
        break;
      }
      i+=1;
    }

    return new Observable<any> ((observer)=> {observer.next({payload:{}})});
  }

  delete(uuid: string) {
    //return this.http.delete('http://localhost:3000/data/' + uuid);

    // return mocked data
    let i = 0;
    for(const element of this.mockedDataMeteoList) {
      if(element.uuid === uuid) {
        this.mockedDataMeteoList.splice(i, 1);
        break;
      }
      i+=1;
    }
        
    return new Observable<any> ((observer)=> {observer.next({payload:{}})});
  }

}
