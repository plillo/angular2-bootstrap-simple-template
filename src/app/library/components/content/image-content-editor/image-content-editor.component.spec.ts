import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ImageContentEditorComponent } from './image-content-editor.component';

describe('ImageContentEditorComponent', () => {
  let component: ImageContentEditorComponent;
  let fixture: ComponentFixture<ImageContentEditorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ImageContentEditorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImageContentEditorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
