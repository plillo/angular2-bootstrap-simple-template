import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FileItem, FileUploader } from 'ng2-file-upload';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { ApplicationService } from 'src/app/base/services/application.service';
import { BackendService } from 'src/app/base/services/backend.service';
import { ContentService } from 'src/app/base/services/rest/content.service';
import { UserService } from 'src/app/base/services/user.service';

export interface UploadItem {
  description: string,
  tags: string,
  topic: string
}

@Component({
  selector: 'app-image-content-editor',
  templateUrl: './image-content-editor.component.html',
  styleUrls: ['./image-content-editor.component.scss']
})
export class ImageContentEditorComponent implements OnInit {
  @Input() topic: string = "";
  @Input() uuid: string = undefined;
  @Input() fieldNameParameter: string = '';
  @Input() fieldNameValue: string = '';
  @Output() operation: EventEmitter<string> = new EventEmitter<string>();

  // File uploader
  // =============
  public uploader: FileUploader;

  // Detail items
  public items: UploadItem[] = [];

  hasBaseDropZoneOver:boolean;

  public content = {
    uuid: undefined,
    url: "",
    topic: "",
    name: "",
    lang: "IT_it",
    body: "",
    tags: "",
    contentType: "",
    description: "",
    URL: undefined
  };

  response: string = '';
  
  constructor(
    private backendService: BackendService, 
    private userService: UserService, 
    private applicationService: ApplicationService, 
    private ngxService: NgxUiLoaderService,
    private contentService: ContentService
    ) { }

  ngOnInit() {
    this.content.topic = this.topic;
    this.getContent();
  }

  ngOnChanges() {
    this.getContent();
  }

  initUploader(): void {
    this.uploader = new FileUploader({
      url: this.backendService.getAuthBackend() + '/core/1.0/users/' +this.userService.getUser().uuid + '/contents/' + this.uuid,
      authToken: `Bearer ${this.userService.getToken()}`,
      disableMultipart: false, // 'DisableMultipart' must be 'true' for formatDataFunction to be called.
      method: 'put',
      itemAlias: 'file',
      allowedFileType: ['image'],
      // inserimento di Tenand-ID e Accept-Language indispensabile perche' si bypassa l'intercettore HTTP
      headers: [
        {name: 'Tenant-ID', value: this.applicationService.getTenantID()},
        {name: 'Accept-Language', value: this.userService.getLocale()}
      ]
    });

    this.uploader.response.subscribe(res => {
      this.response = res;
    });

    this.uploader.onAfterAddingFile = (fileItem: FileItem): any => {
      if(this.uploader.queue.length>1) {
        this.uploader.queue.splice(0, 1); // remove first element
        this.items.splice(0, 1); // remove first element
      }
        
      this.items.push({description:'', tags: '', topic: ''});
    };

    this.uploader.onBeforeUploadItem = (fileItem: FileItem): any => {
      this.ngxService.start();
    }

    this.uploader.onSuccessItem = (fileItem: FileItem): any => {
      this.ngxService.stop();
      if(this.uploader.queue.length>0)
        this.remove(0);

      this.operation.emit('updated');
    }

    this.uploader.onErrorItem = (fileItem: FileItem): any => {
      this.ngxService.stop();
    }

    this.uploader.onBuildItemForm = (fileItem: any, form: any) => {
      if(this.uploader.queue.length==0)
        return this.updateContent();

      for(var k = 0; k < this.uploader.queue.length; k++){
        if(this.uploader.queue[k]==fileItem) {

          form.append('uuid', this.uuid);
          form.append('type', 'file');
          form.append('name', this.content.name);
          form.append('lang', this.content.lang);
          form.append('topic', this.content.topic);
          form.append('tags', this.content.tags);
          form.append('description', this.content.description);
        }
      }  
    };
  }

  getContent(): void {
    if (!this.uuid) return;

    // Initialization of uploader
    this.initUploader();

    this.contentService.get(this.uuid).subscribe(
      (data) => {
        let content = data["content"];

        this.content.uuid = this.uuid;
        this.content.body = content.body;
        this.content.topic = content.topic;
        this.content.name = content.name;
        this.content.lang = content.lang;
        this.content.url = content.url;
        this.content.tags = content.tags;
        this.content.description = content.description;
        this.content.URL = content.URL;
      },
      (error) => {}
    );
  }

  updateContent(): void {
    let formData = new FormData();
    formData.append('uuid', this.uuid);
    formData.append('type', 'file');
    formData.append('name', this.content.name);
    formData.append('lang', this.content.lang);
    formData.append('topic', this.content.topic);
    formData.append('tags', this.content.tags);
    formData.append('description', this.content.description);

    this.contentService.putByFormData(formData).subscribe(
      (data) => {
        let d = data;
        this.operation.emit('updated');
      },
      (error) => {}
    );
  }

  public remove(index) {
    this.uploader.queue[index].remove();
    this.items.splice(index, 1);
  }

  public fileOverBase(e:any):void {
    this.hasBaseDropZoneOver = e;
  }

  isEON(s:string):boolean {
    return (s==undefined || s=='' || s==null);
  }
}
