import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { HttpClient, HttpParams,  } from '@angular/common/http';
import { isNgTemplate } from '@angular/compiler';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { Subject } from 'rxjs';
import { debounceTime } from 'rxjs/operators';
import { BackendService } from 'src/app/base/services/backend.service';
import { UserService } from 'src/app/base/services/user.service';


// selector: "edit-text-content-modal
// ==================================
@Component({
  selector: "edit-text-content-modal",
  templateUrl: "./edit-text-content.component.html",
  styleUrls: ["./edit-text-content.component.scss"],
})
export class EditTextContentModal {
  @Input() uuid;
  @Output() operation: EventEmitter<string> = new EventEmitter<string>();

  constructor(public bsModalRef: BsModalRef) {}

  public ngOnInit(): void {
  }

  editor(event) {
    this.operation.emit(event);
    
    this.bsModalRef.hide();
  }
}

// selector: "edit-image-content-modal
// ==================================
@Component({
  selector: "edit-image-content-modal",
  templateUrl: "./edit-image-content.component.html",
  styleUrls: ["./edit-image-content.component.scss"],
})
export class EditImageContentModal {
  @Input() uuid;
  @Output() operation: EventEmitter<string> = new EventEmitter<string>();

  constructor(public bsModalRef: BsModalRef) {}

  public ngOnInit(): void {
  }

  editor(event) {
    this.operation.emit(event);
    
    this.bsModalRef.hide();
  }
}

@Component({
  selector: 'app-contents-list',
  templateUrl: './contents-list.component.html',
  styleUrls: ['./contents-list.component.scss']
})
export class ContentsListComponent implements OnInit {
  @Input() type: string = '';
  @Input() topic: string = '';

  searchString: string = '';
  filterChanged: Subject<string> = new Subject<string>();

  currentItem: number = undefined;

  contents: any = [];

  constructor(
    private http: HttpClient, 
    private backendService: BackendService, 
    private userService: UserService,
    private modalService: BsModalService) { 
      this.filterChanged.pipe(debounceTime(1000)).subscribe((filter) => {
        this.loadContents();
      });
    }

  fireFilterChanged(filter: string) {
    this.filterChanged.next(filter);
  }

  ngOnInit() {
    this.loadContents();
  }

  loadContents() {
    const url = this.backendService.getAuthBackend() + '/core/1.0/users/' +this.userService.getUser().uuid + '/contents';
    const params = new HttpParams().set('type', this.type).set('topic', this.topic).set('filter', this.searchString);

    this.http.get(url, {params: params})
    .subscribe(
      response=>{
        this.contents = response['contents'];
      }, 
      error=>{
        console.log(error.message);
      })
  }

  download(url, fileName) {
    this.http.get(url, {responseType:'blob'})
        .subscribe(
          response=>{
            var a = document.createElement("a");
            a.href = URL.createObjectURL(response);
            a.download = fileName;
            a.click();
          }, 
          error=>{
            console.log(error.message);
          })
  }

  deleteCurrentItem(index: number) {
    let url = this.backendService.getAuthBackend() + '/core/1.0/users/' +this.userService.getUser().uuid + '/contents/' + this.contents[index].uuid;
    this.http.delete(url)
    .subscribe(
      response=>{
        this.loadContents();
      }, 
      error=>{
        console.log(error.message);
      })
  } 

  editCurrentItem(index: number):void {
    if(this.type=='text') {
      const modalRef = this.modalService.show(EditTextContentModal);
      modalRef.content.uuid = this.contents[index].uuid;
  
      modalRef.content.operation.subscribe((op) => {
        if(op=='updated') {
          this.loadContents();
        }
      });
    }
    else if(this.type=='image') {
      const modalRef = this.modalService.show(EditImageContentModal);
      modalRef.content.uuid = this.contents[index].uuid;
  
      modalRef.content.operation.subscribe((op) => {
        if(op=='updated') {
          this.loadContents();
        }
      });
    }

    this.currentItem = this.currentItem==index?undefined:index;
  }

  isCurrentItem(item: number):boolean {
    return this.currentItem==item;
  }
}


