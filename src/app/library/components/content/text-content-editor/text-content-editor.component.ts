import { Component, EventEmitter, Input, OnChanges, OnInit, Output } from "@angular/core";
import { ContentService } from "src/app/base/services/rest/content.service";

interface UploadResult {
  isImg: boolean;
  name: string;
  url: string;
}

@Component({
  selector: "app-text-content-editor",
  templateUrl: "./text-content-editor.component.html",
  styleUrls: ["./text-content-editor.component.scss"],
})
export class TextContentEditorComponent implements OnInit, OnChanges {
  @Input() topic: string = "";
  @Input() uuid: string = undefined;
  @Output() operation: EventEmitter<string> = new EventEmitter<string>();

  type: string = "txt";

  public content = {
    uuid: undefined,
    url: "",
    type: "text",
    topic: "",
    name: "",
    lang: "IT_it",
    body: "",
    tags: "",
    contentType: "",
    description: "",
    URL: undefined
  };

  options: any;

  constructor(private contentService: ContentService) {}

  ngOnInit() {
    this.content.topic = this.topic;
    this.getContent();
  }

  ngOnChanges() {
    this.getContent();
  }

  doUpload(files: Array<File>): Promise<Array<UploadResult>> {
    // do upload file by yourself
    return Promise.resolve([{ name: "xxx", url: "xxx.png", isImg: true }]);
  }

  preRenderFunc(content: string) {
    return content.replace(/something/g, "new value"); // must return a string
  }

  postRenderFunc(content: string) {
    return content.replace(/something/g, "new value"); // must return a string
  }

  onEditorLoaded(event: any) {
    //TODO
  }

  onPreviewDomChanged(event: any) {
    //TODO
  }

  getContent(): void {
    if (!this.uuid) return;

    this.contentService.get(this.uuid).subscribe(
      (data) => {
        let content = data["content"];

        this.content.uuid = this.uuid;
        this.content.body = content.body;
        this.content.topic = content.topic;
        this.content.name = content.name;
        this.content.lang = content.lang;
        this.content.url = content.url;
        this.content.tags = content.tags;
        this.content.description = content.description;
        this.content.URL = content.URL;
        switch (content.mimeType) {
          case "text/plain":
            this.type = 'text';
            break;
          case "text/html":
            this.type = 'html';
            break;
          case "text/markdown":
            this.type = 'md';
            break;
        }
      },
      (error) => {}
    );
  }

  createContent(): void {
    switch (this.type) {
      case "html":
        this.content.contentType = "text/html";
        break;
      case "md":
        this.content.contentType = "text/markdown";
        break;
      case "txt":
      default:
        this.content.contentType = "text/plain";
        break;
    }

    this.contentService.post(this.content).subscribe(
      (data) => {
        let d = data;
      },
      (error) => {}
    );
  }

  updateContent(): void {
    switch (this.type) {
      case "html":
        this.content.contentType = "text/html";
        break;
      case "md":
        this.content.contentType = "text/markdown";
        break;
      case "txt":
      default:
        this.content.contentType = "text/plain";
        break;
    }

    this.contentService.put(this.content).subscribe(
      (data) => {
        let d = data;
        this.operation.emit('updated');
      },
      (error) => {}
    );
  }
}
