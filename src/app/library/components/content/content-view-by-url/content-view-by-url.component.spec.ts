import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContentViewByUrlComponent } from './content-view-by-url.component';

describe('ContentViewByUrlComponent', () => {
  let component: ContentViewByUrlComponent;
  let fixture: ComponentFixture<ContentViewByUrlComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContentViewByUrlComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContentViewByUrlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
