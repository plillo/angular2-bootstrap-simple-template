import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContentPictureUploadComponent } from './content-picture-upload.component';

describe('ContentPictureUploadComponent', () => {
  let component: ContentPictureUploadComponent;
  let fixture: ComponentFixture<ContentPictureUploadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContentPictureUploadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContentPictureUploadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
