import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FileUploader, FileItem } from 'ng2-file-upload';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { ApplicationService } from 'src/app/base/services/application.service';
import { BackendService } from 'src/app/base/services/backend.service';
import { UserService } from 'src/app/base/services/user.service';

export interface UploadItem {
  description: string,
  tags: string,
  topic: string
}

@Component({
  selector: 'single-file-upload',
  templateUrl: './single-file-upload.component.html',
  styleUrls: ['./single-file-upload.component.scss']
})
export class SingleFileUploadComponent implements OnInit {
  @Input() mode: string = 'details';
  @Input() url: string = ''; 
  @Input() topic: string = '';
  @Input() fieldNameParameter: string = '';
  @Input() fieldNameValue: string = '';

  @Output() confirm: EventEmitter<boolean> = new EventEmitter<boolean>();

  // File uploader
  // =============
  public uploader: FileUploader;
  
  // Detail items
  public items: UploadItem[] = [];

  actualVisibleRowIndex:number = -1;
  hasBaseDropZoneOver:boolean;
  response:string;

  // CONSTRUCTOR
  // ===========
  constructor (
    private backendService: BackendService, 
    private userService: UserService, 
    private applicationService: ApplicationService, 
    private ngxService: NgxUiLoaderService){
      // body
  }

  public isNoDetailsMode(): boolean {
    return 'no-details'==this.mode;
  }

  public selectedFile(): boolean {
    return this.uploader.queue.length>0;
  }

  public fileOverBase(e:any):void {
    this.hasBaseDropZoneOver = e;
  }

  public uploadingURL() {
    return this.backendService.getAuthBackend() + this.url.replace('UUID', this.userService.getUser().uuid);
  }

  public isVisible(index) {
    return index==this.actualVisibleRowIndex;
  }

  public setVisible(index) {
    if(this.actualVisibleRowIndex == index)
      this.actualVisibleRowIndex = -1;
    else 
      this.actualVisibleRowIndex = index;
  }

  public remove(index) {
    this.uploader.queue[index].remove();
    this.items.splice(index, 1);
  }

  ngOnInit() {
    this.uploader = new FileUploader({
      url: this.uploadingURL(),
      authToken: `Bearer ${this.userService.getToken()}`,
      disableMultipart: false, // 'DisableMultipart' must be 'true' for formatDataFunction to be called.
      method: 'post',
      itemAlias: 'picture',
      allowedFileType: ['image', 'pdf', 'doc'],
      // inserimento di Tenand-ID e Accept-Language indispensabile perche' si bypassa l'intercettore HTTP
      headers: [
        {name: 'Tenant-ID', value: this.applicationService.getTenantID()},
        {name: 'Accept-Language', value: this.userService.getLocale()}
      ]
    });
    this.response = '';

    this.uploader.response.subscribe(res => {
      this.response = res;
      this.userService.relogin();
    });

    this.uploader.onAfterAddingFile = (fileItem: FileItem): any => {
      if(this.uploader.queue.length>1) {
        this.uploader.queue.splice(0, 1); // remove first element
        this.items.splice(0, 1); // remove first element
      }
        
      this.items.push({description:'', tags: '', topic: ''});
    };

    this.uploader.onBeforeUploadItem = (fileItem: FileItem): any => {
      this.ngxService.start();
    }

    this.uploader.onSuccessItem = (fileItem: FileItem): any => {
      this.ngxService.stop();
      this.confirm.emit(true);
      if(this.uploader.queue.length>0)
        this.remove(0);
    }

    this.uploader.onErrorItem = (fileItem: FileItem): any => {
      this.ngxService.stop();
      this.confirm.emit(false);
    }

    this.uploader.onBuildItemForm = (fileItem: any, form: any) => {
      let a = fileItem;

      for(var k = 0; k < this.uploader.queue.length; k++){
        if(this.uploader.queue[k]==fileItem) {
          if(!this.isEON(this.items[k].tags)) 
            form.append('tags', this.items[k].tags);
          
          if(!this.isEON(this.topic)) {
            if(!this.isEON(this.items[k].topic)) 
              form.append('topic', this.topic + (this.items[k].topic.startsWith('/') ? this.items[k].topic : '/'+this.items[k].tags));
            else
              form.append('topic', this.topic);
          }

          if(!this.isEON(this.items[k].description)) 
            form.append('description', this.items[k].description);

          if(!this.isEON(this.fieldNameParameter) && !this.isEON(this.fieldNameValue)) 
            form.append(this.fieldNameParameter, this.fieldNameValue);
        }
      }  
    };
  }

  isEON(s:string):boolean {
    return (s==undefined || s=='' || s==null);
  }
}