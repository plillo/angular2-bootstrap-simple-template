import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { FileUploader, FileItem } from 'ng2-file-upload';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { ApplicationService } from 'src/app/base/services/application.service';
import { BackendService } from 'src/app/base/services/backend.service';
import { UserService } from 'src/app/base/services/user.service';

export interface UploadItem {
  name: string,
  originalFileName: string,
  label: string,
  type: string,
  description: string,
  tags: string,
  topic: string
}

@Component({
  selector: 'app-multi-file-upload',
  templateUrl: './multi-file-upload.component.html',
  styleUrls: ['./multi-file-upload.component.scss']
})
export class MultiFileUploadComponent implements OnInit {
  @Input() allowedFileTypes: string[];
  @Input() url: string; 
  @Input() topic: string;
  @Input() defaultName: string;
  @Input() defaultDescription: string;
  @Input() defaultLabel: string;
  @Input() defaultType: string;
  @Input() defaultTags: string;
  @Input() uploadButtonsEnabled: boolean = true;
  @Input() cancelButtonsEnabled: boolean = true;

  @Output() executed = new EventEmitter<boolean>();

  // File uploader
  // =============
  public uploader: FileUploader;
  
  // Detail items
  public items: UploadItem[] = [];

  actualVisibleRowIndex:number = -1;
  hasBaseDropZoneOver:boolean;
  hasAnotherDropZoneOver:boolean;
  response:string;

  constructor (
    private backendService: BackendService, 
    private userService: UserService, 
    private applicationService: ApplicationService,
    private ngxService: NgxUiLoaderService){
      // Constructor body
  }

  public fileOverBase(e:any):void {
    this.hasBaseDropZoneOver = e;
  }

  public fileOverAnother(e:any):void {
    this.hasAnotherDropZoneOver = e;
  }

  public isVisible(index) {
    return index==this.actualVisibleRowIndex;
  }

  public setVisible(index) {
    if(this.actualVisibleRowIndex == index)
      this.actualVisibleRowIndex = -1;
    else 
      this.actualVisibleRowIndex = index;
  }

  public remove(index) {
    this.uploader.queue[index].remove();
    this.items.splice(index, 1);
  }

  public uploadAll(url: string):Promise<string> {
    return new Promise<string>((resolve) => {
      this.url = url;
      if(this.uploader.queue.length>0) {
        this.uploader.uploadAll();
        this.uploader.onCompleteAll = (): any => {
          resolve("all-loaded");
        }
      };
      resolve("no-file-to-load");
    });
  }

  ngOnInit() {
    this.uploader = new FileUploader({
      url: this.url,
      authToken: `Bearer ${this.userService.getToken()}`,
      disableMultipart: false, // 'DisableMultipart' must be 'true' for formatDataFunction to be called.
      method: 'post',
      itemAlias: 'file',
      allowedFileType: this.allowedFileTypes, // p.es. ['image', 'pdf', 'doc']
      // inserimento di Tenand-ID e Accept-Language indispensabile perche' si bypassa l'intercettore HTTP
      headers: [
        {name: 'Tenant-ID', value: this.applicationService.getTenantID()},
        {name: 'Accept-Language', value: this.userService.getLocale()}
      ]
    });

    this.response = '';

    this.uploader.response.subscribe(
      res => {
        this.response = res;
        this.executed.emit(true);
      },
      err => {
        this.response = err;
        this.executed.emit(false);
      }
    );

    this.uploader.onAfterAddingFile = (fileItem: FileItem): any => {
      this.items.push({
        originalFileName:fileItem.file.name, 
        name:this.defaultName?this.defaultName:'', 
        label:this.defaultLabel?this.defaultLabel:'',  
        type:this.defaultType?this.defaultType:'',  
        description:this.defaultDescription?this.defaultDescription:'',  
        tags: this.defaultTags?this.defaultTags:'', 
        topic: ''});
    };

    this.uploader.onBeforeUploadItem = (item) => {
      this.ngxService.start();
      // THIS is the moment to set the URL!!!
      item.url = this.url;
    }

    this.uploader.onSuccessItem = (fileItem: FileItem): any => {
      this.ngxService.stop();

      // remove item
      for(var k = 0; k < this.uploader.queue.length; k++)
        if(this.uploader.queue[k]==fileItem) {
          this.remove(k);
          break;
        }
    };

    this.uploader.onErrorItem = (fileItem: FileItem): any => {
      this.ngxService.stop();
    }

    this.uploader.onBuildItemForm = (fileItem: any, form: any) => {
      let a = fileItem;

      for(var k = 0; k < this.uploader.queue.length; k++){
        if(this.uploader.queue[k]==fileItem) {

          if(!this.isEON(this.items[k].name)) 
          form.append('name', this.items[k].name);

          if(!this.isEON(this.items[k].originalFileName)) 
            form.append('originalFileName', this.items[k].originalFileName);

          if(!this.isEON(this.items[k].label)) 
            form.append('label', this.items[k].label);

          if(!this.isEON(this.items[k].type)) 
            form.append('type', this.items[k].type);

          if(!this.isEON(this.items[k].description)) 
            form.append('description', this.items[k].description);

          if(!this.isEON(this.items[k].tags)) 
            form.append('tags', this.items[k].tags);
          
          if(!this.isEON(this.topic)) {
            if(!this.isEON(this.items[k].topic)) 
              form.append('topic', this.topic + (this.items[k].topic.startsWith('/') ? this.items[k].topic : '/'+this.items[k].tags));
            else
              form.append('topic', this.topic);
          }
        }
      }  
    };
  }

  isEON(s:string):boolean {
    return (s==undefined || s=='' || s==null);
  }
}