import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ParentComponentApi } from 'src/app/components/meteo-list/meteo-list.component';

@Component({
  selector: 'list-paginator',
  templateUrl: './list-paginator.component.html',
  styleUrls: ['./list-paginator.component.scss']
})
export class ListPaginatorComponent {
  @Input() parentApi: ParentComponentApi;
  @Input() pageSize: number;
  @Input() maxPages: number;

  //@Output("getItems") getItems: EventEmitter<any> = new EventEmitter();
  //getItems(from: number, size: number, searchKeywords?: string, genre?: string): void;

  private restService: any = undefined;
  pager: any = {
    pageSize: 10,
    maxPages: 10,
    actualPage: 1,
    firstPage: 1,
    activePage: 1,

    // variables
    itemsNumber: undefined,
    pagesNumber: undefined
  };

  pages: number[] = [];

  constructor() { }

  ngOnInit() {
    this.pager.pageSize = this.pageSize;
    this.pager.maxPages = this.maxPages;

    this.reload();
  }

  setPager(inum:number) {
    this.pager.itemsNumber = inum;
    this.pager.pagesNumber = Math.floor(this.pager.itemsNumber/this.pager.pageSize + (this.pager.itemsNumber%this.pager.pageSize>0?1:0));
    this.setPages();
  }

  reload() {
    this.parentApi.getList((this.pager.activePage-1)*this.pager.pageSize, this.pager.pageSize);
  }

  // PAGER methods
  // =============
  setActivePage(k:number) {
    this.pager.activePage = k;

    this.parentApi.getList(k, this.pager.pageSize);
  }

  setPages() {
    this.pages = [];
    for(let k=this.pager.firstPage; k<=Math.min(this.pager.firstPage+this.pager.maxPages-1,this.pager.pagesNumber); k++)
      this.pages.push(k);
  }

  skipPage(k:number) {
    this.setActivePage(k);
    this.pager.firstPage = Math.floor((k-0.1)/this.pager.maxPages)*this.pager.maxPages+1;

    this.setPages();
  }

  skipPagesBlock(k:number){
    this.pager.firstPage += k*this.pager.maxPages;
    this.setActivePage(this.pager.firstPage);

    this.setPages();
  }

  toSkipPagesBlock(k:number){
    let firstPage = this.pager.firstPage;
    firstPage += k*this.pager.maxPages;

    return firstPage;
  }

  isSkippablePagesBlock(k:number):boolean {
    let to = this.pager.firstPage + k*this.pager.maxPages;
    
    if(to<0 || to>this.pager.pagesNumber) 
      return false;
    return true;
  }

  setFirstPage(k:number) {
    this.pager.firstPage = k;

    this.setActivePage(this.pager.firstPage);
    this.setPages();
  }

  skipLastBlock() {
    let resto = this.pager.pagesNumber % this.pager.maxPages;
    this.pager.firstPage = this.pager.pagesNumber - (resto>0?resto-1:this.pager.maxPages);
    this.setActivePage(this.pager.firstPage);
    this.setPages();
  }

  toSkipLastBlock() {
    let resto = this.pager.pagesNumber % this.pager.maxPages;
    let firstPage = this.pager.pagesNumber - (resto>0?resto-1:this.pager.maxPages);
    return firstPage;
  }
}
