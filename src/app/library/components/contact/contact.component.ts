import { Component, OnInit, Input } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { AlertModalContentComponent } from '../alert-modal-content/alert-modal-content.component';
import { FileUploader, FileItem } from 'ng2-file-upload';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BackendService } from 'src/app/base/services/backend.service';
import { UserService } from 'src/app/base/services/user.service';
import { ApplicationService } from 'src/app/base/services/application.service';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {
  @Input() attachmentEnabled: boolean = false;
  
  focus:boolean = true;

  public sendto: string;
  public name: string;
  public email: string;
  public subject: string;
  public message: string;

  public recipients: any[] = [{code:'info', description:'informazioni generali'},{code:'webmaster', description:'informazioni tecniche'}];

  public uploader: FileUploader;

  constructor(
    private http: HttpClient, 
    private modalService: BsModalService,
    private backendService: BackendService,
    private userService: UserService,
    private applicationService: ApplicationService,
    private ngxService: NgxUiLoaderService) { }

  ngOnInit() {
    this.uploader = new FileUploader({
      url: this.uploadingURL(),
      authToken: `Bearer ${this.userService.getToken()}`,
      disableMultipart: false, // 'DisableMultipart' must be 'true' for formatDataFunction to be called.
      itemAlias: "attachment",
      allowedFileType: ["pdf", "image", "compress", "document"],
      // inserimento di Tenand-ID e Accept-Language indispensabile perche' si bypassa l'intercettore HTTP
      headers: [
        { name: "Tenant-ID", value: this.applicationService.getTenantID() },
        { name: "Accept-Language", value: this.userService.getLocale() },
      ]
    });

    this.uploader.onBeforeUploadItem = (item: FileItem) => {
      item.withCredentials = true;

      item.method = "POST"
      item.url = this.uploadingURL();
      
      this.uploader.options.additionalParameter = {
        name: this.name,
        from: this.email,
        subject: this.subject,
        body: this.message,
        recipient: this.sendto,
      }
    };

    this.uploader.onCompleteAll = () => {
      this.ngxService.stop();
    }
  }

  sendMessage() {
    if(this.uploader.queue.length>0) {
      this.ngxService.start();
      // if selected file then UPLOAD via file-upload plugin
      this.uploader.uploadAll();
    }
    else {
      let formData = new FormData();

      formData.append("name", this.name);
      formData.append("from", this.email);
      formData.append("subject", this.subject);
      formData.append("body", this.message);
      formData.append("recipient", this.sendto);
  
      this.ngxService.start();
      this.http.post(this.backendService.getAuthBackend() + '/contacts/1.0/', formData)
      .subscribe(
          (data) => {
            this.name = '';
            this.email = '';
            this.subject = '';
            this.message = '';
            this.sendto = undefined;
            this.alertModal('Messaggio inviato','Il messaggio e\' stato inviato: riceverai quanto prima una risposta da parte dei nostri operatori.');
          }, 
          (error) => {
  
          }
      )
      .add(()=>{
        this.ngxService.stop();
      });
    }

  }

  public uploadingURL(): string {
    return this.backendService.getAuthBackend() + '/contacts/1.0/';
  }

  public attachedChanged(): void {
    if(this.uploader.queue.length>1)
      this.uploader.queue.splice(0, 1); // remove first element
  }
  
  alertModal(title: string, text: string): void {
    const modalRef = this.modalService.show(AlertModalContentComponent);
    modalRef.content.title = title;
    modalRef.content.text = text;
  }

}

