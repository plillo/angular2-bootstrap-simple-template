import { CommonModule } from "@angular/common";
import { HttpClient } from "@angular/common/http";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";
import { JwBootstrapSwitchNg2Module } from "jw-bootstrap-switch-ng2";
import { FileUploadModule } from "ng2-file-upload";
import { BsDatepickerModule } from "ngx-bootstrap/datepicker";
import { BsDropdownModule } from "ngx-bootstrap/dropdown";
import { TabsModule } from "ngx-bootstrap/tabs";
import { MarkdownModule, MarkedOptions, MarkedRenderer } from "ngx-markdown";
import { LMarkdownEditorModule } from "ngx-markdown-editor";
import { ConfirmationModalContent } from "../base/components/settings/settings.component";
import { AlertModalContentComponent } from "./components/alert-modal-content/alert-modal-content.component";
import { ContactComponent } from "./components/contact/contact.component";
import { ContentMenuComponent } from "./components/content/content-menu/content-menu.component";
import { ContentPictureUploadComponent } from "./components/content/content-picture-upload/content-picture-upload.component";
import { ContentViewByUrlComponent } from "./components/content/content-view-by-url/content-view-by-url.component";
import { ContentViewComponent } from "./components/content/content-view/content-view.component";
import { ContentsListComponent, EditImageContentModal, EditTextContentModal } from "./components/content/contents-list/contents-list.component";
import { ImageContentEditorComponent } from "./components/content/image-content-editor/image-content-editor.component";
import { TextContentEditorComponent } from "./components/content/text-content-editor/text-content-editor.component";
import { MultiFileUploadComponent } from "./components/multi-file-upload/multi-file-upload.component";
import { SingleFileUploadComponent } from "./components/single-file-upload/single-file-upload.component";
import { SafePipe } from "./pipes/safe";
import { ListPaginatorComponent } from './components/list-paginator/list-paginator.component';

// ngx-markdown renderer
// =====================
// function that returns `MarkedOptions` with renderer override
export function markedOptionsFactory(): MarkedOptions {
  const renderer = new MarkedRenderer();

  renderer.image = function(href, title, text) {    
    let size: string = '';
    let clazz: string  = '';
    let style: string  = '';
    if (title) {
      let splittedTitle: string[] = title.split(';');
      splittedTitle.forEach(function (item, index) {
        let token = item.trim();

        if(token.startsWith('class=')) {
          clazz = 'class="'+token.substring(6)+'"';
        }
        else if(token.startsWith('style=')) {
          style = 'style="'+token.substring(6)+'"';
        }
        else {
          let splitted: string[] = token.split('x');                                                                                          
          if (splitted[1]) {  
              if(splitted[0].trim().length>0 && parseInt(splitted[0])!=NaN) size += 'width=' + splitted[0].trim();
              if(splitted[1].trim().length>0 && parseInt(splitted[1])!=NaN) size += (size.length>0?' ':'')+'height=' + splitted[1].trim();
          } else {    
              if(splitted[0].trim().length>0 && parseInt(splitted[0])!=NaN) size += 'width=' + splitted[0].trim();                                                                                      
          }   
        } 
      });
    }

    let attributes = '';
    if(clazz.length>0)
      attributes += ' '+clazz;
    if(style.length>0)
      attributes += ' '+style;
    if(size.length>0)
      attributes += ' '+size;

    return ('<img src="' + href + '" alt="' + text + '"' + attributes + ' />');                                                        
  };

  return {
    renderer: renderer,
    gfm: true,
    breaks: false,
    pedantic: false,
    smartLists: true,
    smartypants: false,
  };
}

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        RouterModule,
        FontAwesomeModule,
        BsDropdownModule,
        BsDatepickerModule,
        MarkdownModule,
        LMarkdownEditorModule,
        JwBootstrapSwitchNg2Module,
        FileUploadModule,
        TabsModule.forRoot(),
        MarkdownModule.forRoot({ 
          loader: HttpClient,
          markedOptions: {
            provide: MarkedOptions,
            useFactory: markedOptionsFactory,
          }
        })
    ],
    declarations: [
        SingleFileUploadComponent,
        MultiFileUploadComponent,
        ContentsListComponent,
        ContentMenuComponent,
        ContentViewComponent,
        ContentViewByUrlComponent,
        TextContentEditorComponent,
        ImageContentEditorComponent,
        ContentPictureUploadComponent,
        EditTextContentModal,
        EditImageContentModal,
        ContactComponent,
        ConfirmationModalContent,
        AlertModalContentComponent,
        ListPaginatorComponent,
        SafePipe
    ],
    providers: [
    ],
    entryComponents: [
      AlertModalContentComponent
    ],
    exports:[ 
        SingleFileUploadComponent,
        MultiFileUploadComponent,
        ContactComponent,
        EditTextContentModal,
        EditImageContentModal,
        SafePipe,
        ListPaginatorComponent
    ]
})
export class LibraryModule { }