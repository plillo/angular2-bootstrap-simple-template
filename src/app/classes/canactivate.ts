import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { LocalStorageService } from 'angular-2-local-storage';
import { UserService } from '../base/services/user.service';

@Injectable()
export class CanActivateViaAuthGuard implements CanActivate {

  constructor(private router: Router, private localStorage: LocalStorageService, private userService: UserService) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if(this.userService.isUserLogged() || "/signup"==this.router.url)
        return true;
    else {
        this.localStorage.set("returnURL", state.url);
        this.router.navigateByUrl("/signup");
        return false;
    }
  }
}