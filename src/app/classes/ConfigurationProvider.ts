import { Injectable } from "@angular/core";

@Injectable()
export class ConfigurationProvider {
    private productionEnvironment: boolean = true;
    private applicationCode: string = null;
    private tenantID: string = null;

    constructor() {
    }

    public isProductionEnvironment(): boolean {
        return this.productionEnvironment;
    }

    public getApplicationCode(): string {
        return this.applicationCode;
    }

    public getTenantID(): string {
        return this.tenantID;
    }

    load() {
        console.log("--> configuration provider");
        return new Promise((resolve, reject) => {
            this.productionEnvironment = true;
            this.applicationCode = 'salentopoli';
            this.tenantID = 'salentopoli';

            resolve(true);
        })
    }
}