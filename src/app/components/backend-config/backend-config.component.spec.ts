import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BackendConfigComponent } from './backend-config.component';

describe('BackendConfigComponent', () => {
  let component: BackendConfigComponent;
  let fixture: ComponentFixture<BackendConfigComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BackendConfigComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BackendConfigComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
