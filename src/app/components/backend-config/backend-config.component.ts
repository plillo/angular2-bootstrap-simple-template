import { Component, OnInit } from '@angular/core';
import { LocalStorageService } from 'angular-2-local-storage';
import { BackendService } from 'src/app/base/services/backend.service';

@Component({
  selector: 'app-backend-config',
  templateUrl: './backend-config.component.html',
  styleUrls: ['./backend-config.component.scss']
})
export class BackendConfigComponent implements OnInit {
  public productionAuthBackend: string;
  public productionBlogBackend: string;
  public productionApplicationBackend: string;

  public developmentAuthBackend: string;
  public developmentBlogBackend: string;
  public developmentApplicationBackend: string;

  public mockingState: boolean;

  constructor(private localStorage: LocalStorageService, private backendService: BackendService) { }

  ngOnInit() {
    this.mockingState = this.localStorage.get("mockingMode")?true:false;

    this.productionAuthBackend = this.backendService.getProductionAuthBackend();
    this.productionBlogBackend = this.backendService.getProductionBlogBackend();
    this.productionApplicationBackend = this.backendService.getProductionApplicationBackend();

    let fromStorage: string = this.localStorage.get("developmentAuthBackend");
    if(fromStorage) {
      this.developmentAuthBackend = fromStorage
    }
    else {
      this.developmentAuthBackend = '#';
    }

    fromStorage = this.localStorage.get("developmentBlogBackend");
    if(fromStorage) {
      this.developmentBlogBackend = fromStorage
    }
    else {
      this.developmentBlogBackend = '#';
    }

    fromStorage = this.localStorage.get("developmentApplicationBackend");
    if(fromStorage) {
      this.developmentApplicationBackend = fromStorage
    }
    else {
      this.developmentApplicationBackend = '#';
    }
  }

  developmentAuthBackendChanged(evt) {
    this.localStorage.set("developmentAuthBackend", this.developmentAuthBackend);
  }

  developmentBlogBackendChanged(evt) {
    this.localStorage.set("developmentBlogBackend", this.developmentBlogBackend);
  }

  developmentApplicationBackendChanged(evt) {
    this.localStorage.set("developmentApplicationBackend", this.developmentApplicationBackend);
  }

  setDevelopmentAuthBackendActive(status) {
    this.localStorage.set("developmentAuthBackendStatus", status);
  }

  setDevelopmentBlogBackendActive(status) {
    this.localStorage.set("developmentBlogBackendStatus", status);
  }

  setDevelopmentApplicationBackendActive(status) {
    this.localStorage.set("developmentApplicationBackendStatus", status);
  }

  isDevelopmentAuthBackendActive() {
    status = this.localStorage.get("developmentAuthBackendStatus");
    if(status) return "true"==status;

    return false;
  }

  isDevelopmentBlogBackendActive() {
    status = this.localStorage.get("developmentBlogBackendStatus");
    if(status) return "true"==status;

    return false;
  }

  isDevelopmentApplicationBackendActive() {
    status = this.localStorage.get("developmentApplicationBackendStatus");
    if(status) return "true"==status;

    return false;
  }

  toggleMockingState() {
    this.localStorage.set("mockingMode", !this.localStorage.get("mockingMode"));
    this.mockingState = this.localStorage.get("mockingMode");
  }
}