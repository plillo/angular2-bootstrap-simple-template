import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { NgxUiLoaderService } from 'ngx-ui-loader';

interface Link {
    id: number;
    name: string;
    routerLink: string;
    description?: string;
}

@Component({
  selector: 'app-list-example',
  templateUrl: './list-example.component.html',
  styleUrls: ['./list-example.component.scss']
})
export class ListExampleComponent implements OnInit {
  public items: Link[];

  constructor(private http: HttpClient, private ngxService: NgxUiLoaderService) { }

  ngOnInit() {
    this.getItems();
  }

  getItems() {
    this.ngxService.start();
      this.http.get("../assets/data/items.json").subscribe(
        (data:Link[]) => {
          this.ngxService.stop();
          this.items = data;
        },
        (error) => {
          this.ngxService.stop();
          console.log(error);
        }
      );
      console.log(this.items);
  }
}