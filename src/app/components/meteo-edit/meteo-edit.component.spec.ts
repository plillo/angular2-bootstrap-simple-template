import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MeteoEditComponent } from './meteo-edit.component';

describe('MeteoEditComponent', () => {
  let component: MeteoEditComponent;
  let fixture: ComponentFixture<MeteoEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MeteoEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MeteoEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
