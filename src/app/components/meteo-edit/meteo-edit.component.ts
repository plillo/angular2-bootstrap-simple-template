import { HttpClient } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { RestMeteoService } from 'src/app/services/rest-meteo.service';

@Component({
  selector: 'app-meteo-edit',
  templateUrl: './meteo-edit.component.html',
  styleUrls: ['./meteo-edit.component.scss']
})
export class MeteoEditComponent implements OnInit {
  @Input() uuid: string = undefined;
  focus: boolean = false;

  public data: {
    uuid: string;
    date: Date;
    air: {
      temp: number;
      humidity: number;
    },
    wind:{
      degrees: number;
      velocity: number;
    }   
  } = { uuid: undefined, date: undefined, air: {temp:undefined, humidity: undefined}, wind: {degrees:undefined, velocity: undefined}};

  constructor(private activatedRoute: ActivatedRoute, private router: Router, private service: RestMeteoService, private ngxService: NgxUiLoaderService) { }

  ngOnInit() {
    this.activatedRoute.params.subscribe(params => { // use 'queryParamas' in place of 'params' if params are in queryString
      if(params['dataUuid']) {
        this.uuid = params['dataUuid'];

        this.service.get(this.uuid).subscribe(
          (data: any) => {
            this.data = data.item;
          },
          (error) => {
            let e = error;
          }
        );
      }
    });
  }
  
  insert() {
    this.data.date = new Date();
    
    this.ngxService.start();
    this.service.post(this.data).subscribe(
      (response: any)=> {
        this.ngxService.stop();
        this.resetData();
      },
      (error: any)=> {
        this.ngxService.stop();
        console.log("Error:", error);
      }
    );
  }

  update() {
    this.ngxService.start();
    this.service.put(this.data.uuid, this.data).subscribe(
      (response: any)=> {
        this.ngxService.stop();
      },
      (error: any)=> {
        this.ngxService.stop();
        console.log("Error:", error);
      }
    );
  }

  resetData() {
    this.data = { uuid: undefined, date: undefined, air: {temp:undefined, humidity: undefined}, wind: {degrees:undefined, velocity: undefined}};
  }
  
}
