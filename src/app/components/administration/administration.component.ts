import { Component, OnInit, OnDestroy } from '@angular/core';
import { CollapsedMenuService } from 'src/app/base/services/collapsed-menu.service';
import { UserService } from 'src/app/base/services/user.service';


@Component({
  selector: 'app-administration',
  templateUrl: './administration.component.html',
  styleUrls: ['./administration.component.scss']
})
export class AdministrationComponent  implements OnInit, OnDestroy {
  // Level '2' collapsed menu items
  public items: any[] = [
    // ==============
    // Configurazioni
    {
      level: 2,
      label: "Configurazioni",
      group: 'configurations'
    },
    {
      level: 2,
      sublevel: 1,
      groupMaster: 'configurations',
      routerLink: ['/administration/backend'],
      label: "REST End Points",
    }
  ];

  constructor(private userService: UserService, private collapsedMenuService: CollapsedMenuService) { }

  ngOnInit() {
    // ADD items to the collapsed menu service
    this.collapsedMenuService.addItems(this.items, 2, 'Amministrazione');
  }

  ngOnDestroy(): void {
      // ADD items to the collapsed menu service
      this.collapsedMenuService.removeItems(2);
  }

  isUserAdmin(): boolean {
    return this.userService.isUserInOneOfRoles('admin, root');
  } 

  isUserLogged(): boolean {
    return this.userService.isUserLogged();
  }

  sidebarToggle() {
    this.collapsedMenuService.sidebarToggle();
  }
}

