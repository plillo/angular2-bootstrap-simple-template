import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PixiExampleComponent } from './pixi-example.component';

describe('PixiExampleComponent', () => {
  let component: PixiExampleComponent;
  let fixture: ComponentFixture<PixiExampleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PixiExampleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PixiExampleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
