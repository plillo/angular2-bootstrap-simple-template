import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-alert-modal-content',
  templateUrl: './alert-modal-content.component.html',
  styleUrls: ['./alert-modal-content.component.scss']
})
export class AlertModalContentComponent implements OnInit {
  title: string;
  text: string;
  confirmButtonText: string;
  cancelButtonText: string;

  @Output() confirm: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor(public bsModalRef: BsModalRef) {}

  ngOnInit() {
  }

  closeEvent(result: boolean) {
    this.bsModalRef.hide();
    this.confirm.emit(result);
  }
}
