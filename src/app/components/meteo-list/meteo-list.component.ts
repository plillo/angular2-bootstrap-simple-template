import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { ListPaginatorComponent } from 'src/app/library/components/list-paginator/list-paginator.component';
import { RestMeteoService } from 'src/app/services/rest-meteo.service';

export interface SensorData {
  uuid: String,
  date: Date,
  air: { temp:number, humidity: string},
  wind: { degrees:string, velocity: string}
}

@Component({
  selector: 'app-meteo-list',
  templateUrl: './meteo-list.component.html',
  styleUrls: ['./meteo-list.component.scss']
})
export class MeteoListComponent implements OnInit {
  @ViewChild(ListPaginatorComponent, { static: true }) paginator: ListPaginatorComponent;

  public list:SensorData[] = [];

  constructor(private service: RestMeteoService, private ngxService: NgxUiLoaderService, private router: Router) {}

  ngOnInit() {
    //this.getList(0, 10); // inizializzazione delegata al paginatore
  }

  // interazione con il paginatore: restituisce oggetto con metodo di esecuzione di getList (metodo locale)
  // ======================================================================================================
  getParentApi(): ParentComponentApi {
    return {
      getList: (from: number, size:number) => {
        this.getList(from, size);
      }
    }
  }

  getList(from: number, size: number) {
    this.ngxService.start();
    this.service.getList().subscribe(
      (response: any)=> {
        this.ngxService.stop();
        // la risposta ha due parametri: una pagina di elementi della lista e il numero totale degli elementi
        this.list = response.list;
        // set/reset del paginatore
        //TODO valutare se si riesce a spostare questa logica nel paginatore restituendo una Promise (Observable)
        this.paginator.setPager(response.count);
      },
      (error: any)=> {
        this.ngxService.stop();
        console.log("Error:", error);
      }
    );
  }

  goto(link):any {
    this.router.routeReuseStrategy.shouldReuseRoute = function () {
      return false;
    };
    this.router.navigate([link]);
  }

  edit(uuid: string) {
    this.router.navigateByUrl("/meteo-edit;dataUuid="+uuid);
  }

  delete(uuid: string) {
    this.ngxService.start();
    this.service.delete(uuid).subscribe(
      (data: any) => {
        this.ngxService.stop();
        // trigger al paginatore per il reload della lista (il paginatore richiamera' getList)
        this.paginator.reload();
      },
      (error: any) => {
        this.ngxService.stop();
      }
    );
  }
}

export interface ParentComponentApi {
  getList: (from: number, size: number) => void
}
