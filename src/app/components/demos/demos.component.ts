import { Component, OnInit, OnDestroy } from '@angular/core';
import { CollapsedMenuService } from 'src/app/base/services/collapsed-menu.service';

@Component({
  selector: 'app-aic-net',
  templateUrl: './demos.component.html',
  styleUrls: ['./demos.component.scss']
})
export class DemosComponent implements OnInit, OnDestroy {
  // Level '2' collapsed menu items
  public items: any[] = [
    // =========
    // Componenti
    {
      level: 2,
      label: 'ngx-bootstrap',
      group: 'bootstrap'
    },
    {
      level: 2,
      sublevel: 1,
      groupMaster: 'bootstrap',
      routerLink: ['/demo/dates'],
      label: "Dates",
    },
    {
      level: 2,
      sublevel: 1,
      groupMaster: 'bootstrap',
      routerLink: ['/demo/timepicker'],
      label: "Time picker",
    },
    {
      level: 2,
      sublevel: 1,
      groupMaster: 'bootstrap',
      routerLink: ['/demo/buttons'],
      label: "Buttons",
    },
  ];  

  constructor(private collapsedMenuService: CollapsedMenuService) { }

  ngOnInit() {
    // ADD items to the collapsed menu service
    this.collapsedMenuService.addItems(this.items, 2, 'Componenti');
  }

  ngOnDestroy(): void {
      // ADD items to the collapsed menu service
      this.collapsedMenuService.removeItems(2);
  }

  sidebarToggle() {
    this.collapsedMenuService.sidebarToggle();
  }

}

