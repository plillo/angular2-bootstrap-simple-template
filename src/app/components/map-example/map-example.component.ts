import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { UserService } from 'src/app/base/services/user.service';
import { MouseEvent } from '@agm/core';
import { MapExampleService } from 'src/app/services/map-example.service';

interface IMarker {
	lat: number;
	lng: number;
	label?: any;
  draggable: boolean;
  name?: string;
}

@Component({
  selector: 'app-map-example',
  templateUrl: './map-example.component.html',
  styleUrls: ['./map-example.component.scss']
})
export class MapExampleComponent implements OnInit {
  focus:boolean = true;
  latitude: number = 41.80;
  longitude: number = 12.49;
  zoom:number = 5;

  // List of all Giots
  giots: any[];

  markers: IMarker[] = [];
  map: any;

  constructor(
      private userService: UserService,
      private itemService: MapExampleService,
      public location: Location) { 
    this.itemService.getAll().subscribe(      
      response=>{
        this.giots = response['giots'];
        for(let giot of this.giots) {
          if(giot.position) {
            let marker: IMarker = {
              lat: giot.position.coordinates.lat,
              lng: giot.position.coordinates.lng,
              name: giot.name,
              label: {text: giot.location, fontSize: '12px', fontWeight: 'bold', backgroundColor: 'white'},
              draggable: false 
            }
            this.markers.push(marker);
          }
        }
      }, 
      error=>{
        console.log(error.message);
      });
  }

  clickedMarker(label: string, index: number) {
    console.log(`clicked the marker: ${label || index}`)
  }

  zoomItem(index: number) {
    //set latitude, longitude and zoom
    this.latitude = this.markers[index].lat;
    this.longitude = this.markers[index].lng;
    this.zoom = 15;
  }
  
  mapClicked($event: MouseEvent) {
    this.markers.push({
      lat: $event.coords.lat,
      lng: $event.coords.lng,
      draggable: true
    });
  }

  mapReset() {
    this.latitude = 41.80;
    this.longitude = 12.49;
    this.zoom = 5;
  }

  mapReady(map) {
    this.map = map;
    map.addListener("dragend", (e: any) => {
      this.latitude = this.map.center.lat();
      this.longitude = this.map.center.lng();
    });
  }

  centerChange(e: any) {
    this.latitude = e.lat;
    this.longitude = e.lng;
  }
  
  markerDragEnd(m: IMarker, $event: MouseEvent) {
    console.log('dragEnd', m, $event);
  }

  isUserLogged(): boolean {
    return this.userService.isUserLogged();
  }

  isUserAdmin() {
    return this.userService.isUserInOneOfRoles('admin, root');
  }

  ngOnInit() {
  }

}


