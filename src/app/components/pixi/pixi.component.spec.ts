import { TestBed, async } from '@angular/core/testing';
import { PixiComponent } from './pixi.component';
describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        PixiComponent
      ],
    }).compileComponents();
  }));
  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(PixiComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));
  it(`should have as title 'angular-pixi-cubicbezier'`, async(() => {
    const fixture = TestBed.createComponent(PixiComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('angular-pixi-cubicbezier');
  }));
  it('should render title in a h1 tag', async(() => {
    const fixture = TestBed.createComponent(PixiComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('h1').textContent).toContain('Welcome to angular-pixi-cubicbezier!');
  }));
});
