import { AgmCoreModule } from "@agm/core";
import { AgmJsMarkerClustererModule } from "@agm/js-marker-clusterer";
import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";
import { BsDropdownConfig, BsDropdownModule } from "ngx-bootstrap/dropdown";
import { TabsModule } from "ngx-bootstrap/tabs";
import { LibraryModule } from "../library/library.module";
import { ProfileComponent } from "./components/profile/profile.component";
import { SettingsComponent } from "./components/settings/settings.component";
import { CollapsedMenuComponent } from "./components/layout/collapsed-menu/collapsed-menu.component";
import { FooterComponent } from "./components/layout/footer/footer.component";
import { NavbarComponent } from "./components/layout/navbar/navbar.component";
import { SignupComponent } from "./components/signup/signup.component";

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        RouterModule,
        FontAwesomeModule,
        TabsModule.forRoot(),
        BsDropdownModule.forRoot(),
        AgmCoreModule.forRoot({
            apiKey: 'AIza...', // https://developers.google.com/maps/documentation/javascript/get-api-key
            libraries: ['places']
          }),
        AgmJsMarkerClustererModule,
        LibraryModule
    ],
    declarations: [
        ProfileComponent,
        SettingsComponent,
        SignupComponent,
        NavbarComponent,
        FooterComponent,
        CollapsedMenuComponent,
    ],
    providers: [
        // ngx-bootstrap configs
        BsDropdownConfig
    ],
    entryComponents: [
    ],
    exports:[
        NavbarComponent,
        FooterComponent
    ]
})
export class ProfileModule { }