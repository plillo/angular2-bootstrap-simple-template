import { Component, Input, OnInit, Output, EventEmitter } from "@angular/core";
import { Subject } from "rxjs";
import { debounceTime } from "rxjs/operators";
import { BsModalService, BsModalRef } from "ngx-bootstrap/modal";
import { Router } from "@angular/router";
import { HttpClient } from "@angular/common/http";
import { LocalStorageService } from "angular-2-local-storage";
import { ConfigurationProvider } from "src/app/classes/ConfigurationProvider";
import { UserService } from "../../services/user.service";
import { AlertModalContentComponent } from "src/app/library/components/alert-modal-content/alert-modal-content.component";

interface FlagOption {
  uuid: string;
  name: string;
  description: string;
  labelID: string;
  mandatory: boolean;

  value: boolean;
}

// privacy modal
// =============
@Component({
  selector: "app-modal-content",
  templateUrl: "./privacy.component.html",
  styleUrls: ["./privacy.component.scss"],
})
export class PrivacyModalContent implements OnInit {
  public text: string;
  public onClose: Subject<boolean>;

  constructor(public _bsModalRef: BsModalRef) {}

  ngOnInit(): void {
    this.onClose = new Subject();
  }

  public onConfirm(): void {
    this.onClose.next(true);
    this._bsModalRef.hide();
  }

  public onCancel(): void {
    this.onClose.next(false);
    this._bsModalRef.hide();
  }
}

// not in production modal
// =======================
@Component({
  selector: "app-modal-nip",
  templateUrl: "./notinproduction.component.html",
  styleUrls: ["./notinproduction.component.scss"],
})
export class NotInProductionModalContent {
  public text: string;

  constructor(public bsModalRef: BsModalRef) {}
}

// confirmation modal
// ==================
@Component({
  selector: "app-signup-confirmation",
  templateUrl: "./confirmation.component.html",
  styleUrls: ["./confirmation.component.scss"],
})
export class SignupConfirmationModalContent {
  @Input() email;
  @Input() mobile;
  @Input() waitingForEmailConformation = false;
  @Input() waitingForMobileConformation = false;
  confirmationCode: string;
  badCode: boolean = false;

  @Output() change: EventEmitter<string> = new EventEmitter<string>();
  @Output() close: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor(public bsModalRef: BsModalRef) {}

  closeEvent() {
    this.bsModalRef.hide();
    this.close.emit(true);
  }

  fireEvent() {
    this.badCode = false;
    this.change.emit(this.confirmationCode);
  }
}

@Component({
  selector: "app-signup",
  templateUrl: "./signup.component.html",
  styleUrls: ["./signup.component.scss"],
})
export class SignupComponent implements OnInit {
  nowDate: Date = new Date();

  userEmail: string = "";
  userPassword: string = "";
  userConfirmPassword: string;

  userEmailChanged: Subject<string> = new Subject<string>();
  processing: boolean;

  privacy: boolean = false;

  isValidIdentificator: boolean = false;
  isLoggingIn: boolean = false;
  isSigningin: boolean = false;

  validPassword: boolean = false;
  validPasswords: boolean = false;
  showPassword = false;

  passwordValidationMessages: string[] = undefined;

  privacyFlags: FlagOption[] = [];

  // Environmental properties
  // ========================
  productionEnvi: boolean;

  constructor(
    private userService: UserService,
    private configurationProvider: ConfigurationProvider,
    private http: HttpClient,
    private modalService: BsModalService,
    private router: Router,
    private localStorage: LocalStorageService
  ) {
    this.userEmail = "";
    this.userPassword = "";

    this.productionEnvi = configurationProvider.isProductionEnvironment();

    this.userEmailChanged.pipe(debounceTime(1500)).subscribe((model) => {
      console.log("validating identificator: " + model);
      this.processing = true;
      userService
        .validateIdentificator(model)
        .then((res) => {
          this.processing = false;

          this.isValidIdentificator = res["isValid"];
          this.isLoggingIn = res["matched"];

          if (this.isValidIdentificator && !this.isLoggingIn) {
            if (this.productionEnvi)
              this.isSigningin = this.isValidIdentificator && !this.isLoggingIn;
            else this.openNotInProduction();
          }

          console.log("Validated identificator: " + JSON.stringify(res));
        })
        .catch((err) => {
          this.processing = false;
          console.log("Error validating identificator: " + JSON.stringify(err));
        });
    });
  }

  ngOnInit() {
    this.userService.getOptions().then(
      (options: any[]) => {
        for (let k = 0; k < options.length; k++) {
          this.privacyFlags.push({
            uuid: options[k].uuid,
            name: options[k].name,
            description: options[k].description,
            labelID: options[k].labelID,
            mandatory: options[k].mandatory,
            value: false,
          });
        }
      },
      (error) => {}
    );
  }

  alertModal(title: string, text: string): void {
    const modalRef = this.modalService.show(AlertModalContentComponent);
    modalRef.content.title = title;
    modalRef.content.text = text;
  }

  login() {
    this.userService
      .login(this.userEmail, this.userPassword)
      .then((res) => {
        this.processing = false;
        let returnURL: string = this.localStorage.get("returnURL");
        if (returnURL) {
          this.localStorage.remove("returnURL");
          this.router.navigateByUrl(returnURL);
        } else if (this.userService.getUser().isTrustedEmail) {
          this.router.navigateByUrl("/home");
        } else {
          this.router.navigateByUrl("/profile");
        }
      })
      .catch((err) => {
        this.processing = false;
        this.validPassword = false;
        console.log("Error logging user: " + err);
      });
    console.log("Logging user...");
  }

  register() {
    this.userService
      .signin(this.userEmail, this.userPassword, this.privacyFlags)
      .then((res) => {
        this.openEmailModal();
        this.processing = false;
      })
      .catch((err) => {
        this.processing = false;
        this.validPassword = false;
        console.log("Error signing-in user: " + err);
      });
    console.log("Signing-in user...");
  }

  emailChanged(email: string) {
    this.userEmailChanged.next(email);
  }

  passwordChanged() {
    if (this.isLoggingIn) {
      this.validPassword = true;
      return;
    }

    let response = this.userService.isValidPassword(this.userPassword);
    this.validPassword = response.isvalid;
    this.passwordValidationMessages = response.messages;

    this.passwordsChanged();
  }

  passwordsChanged() {
    this.validPasswords =
      this.validPassword && this.userPassword == this.userConfirmPassword;
  }

  isValidPassword(): any {
    return this.userService.isValidPassword(this.userPassword).isvalid;
  }

  forgottenPassword(): void {
    this.processing = true;
    this.userService
      .forgottenPassword(this.userEmail)
      .then((res) => {
        this.processing = false;

        this.alertModal(
          "Procedura di accesso temporaneo",
          "Inviata all'indirizzo di posta elettronica " +
            this.userEmail +
            " una password provvisoria utilizzabile una sola volta per l'accreditamento"
        );
      })
      .catch((err) => {
        this.processing = false;
        console.log("Forgotten password error: " + err);
      });
    console.log("Forgotten password process started");
  }

  getTextButton(): string {
    return this.isLoggingIn
      ? this.validPassword
        ? "Log-In"
        : ""
      : this.isSigningin
      ? !this.privacy
        ? "Lettura informativa"
        : !this.privacyAcceptance()
        ? ""
        : this.validPasswords
        ? "Registrazione"
        : ""
      : "";
  }

  getAction(): string {
    return this.isLoggingIn
      ? this.validPassword
        ? "login"
        : "no"
      : this.isSigningin
      ? !this.privacy
        ? "privacy"
        : this.validPasswords && this.privacyAcceptance()
        ? "signin"
        : ""
      : "no";
  }

  privacyAcceptance(): boolean {
    for (let k = 0; k < this.privacyFlags.length; k++) {
      if (this.privacyFlags[k].mandatory && !this.privacyFlags[k].value)
        return false;
    }

    return true;
  }

  getText(): string {
    return "text";
  }

  changeShowPassword(): void {
    this.showPassword = !this.showPassword;
  }

  action() {
    if (this.getAction() === "privacy") this.open();
    else
      switch (this.getAction()) {
        case "signin":
          this.register();
          break;
        case "login":
          this.login();
          break;
      }
  }

  open() {
    const modalRef = this.modalService.show(PrivacyModalContent);
    modalRef.setClass('modal-style');
    modalRef.content.onClose.subscribe((result) => {
      if (result === true) this.privacy = true;
    });

    // Set modal content.text getting privacy.html from assets
    this.http
      .get("./assets/html/privacy.html", { responseType: "text" })
      .subscribe(
        (data) => {
          modalRef.content.text = data;
        },
        (err) => {
          modalRef.content.text = err;
        }
      );
  }

  openNotInProduction() {
    const modalRef = this.modalService.show(NotInProductionModalContent);
    modalRef.content.text = "Procedura di registrazione momentaneamente disattivata per un aggiornamento della piattaforma.";
    /*
    modalRef.content.then(
      (result) => {},
      (reason) => {}
    );
    */
  }

  // MODAL
  // =====
  openEmailModal() {
    let config = {
      backdrop: true,
      ignoreBackdropClick: true
    }

    const modalRef = this.modalService.show(SignupConfirmationModalContent, config);
    modalRef.content.email = this.userService.getUser().email;
    modalRef.content.waitingForEmailConformation = true;

    modalRef.content.close.subscribe(() => {
      this.router.navigateByUrl("/home");
    });

    modalRef.content.change.subscribe((code) => {
      // Verify the confirmation code sent to e-mail address
      this.userService
        .validateEMail(code)
        .then((confirmed) => {
          if (confirmed) {
            this.modalService.hide();
            this.userService.relogin();
            this.router.navigateByUrl("/home");
          }
          else {
            modalRef.content.badCode = true;
          }
        })
        .catch((err) => {});
    });
  }
}
