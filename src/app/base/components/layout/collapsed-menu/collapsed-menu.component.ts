import { Component, OnInit } from '@angular/core';
import { CollapsedMenuService } from 'src/app/base/services/collapsed-menu.service';

@Component({
  selector: 'app-collapsed-menu',
  templateUrl: './collapsed-menu.component.html',
  styleUrls: ['./collapsed-menu.component.scss']
})
export class CollapsedMenuComponent implements OnInit {
  toggleButton: any;
  constructor(
    private collapsedMenuService: CollapsedMenuService
  ) { }

  ngOnInit() {
  }

  itemsLevel(level: number): any[]  {
    return this.collapsedMenuService.getItems(level);
  }

  toggleGroup(masterGroup: string): void {
    this.collapsedMenuService.toggleGroup(masterGroup);
  }

  getLabel(level: number): string {
    return this.collapsedMenuService.getLabel(level);
  }

  sidebarToggle() {
    this.collapsedMenuService.sidebarToggle();
  }
}