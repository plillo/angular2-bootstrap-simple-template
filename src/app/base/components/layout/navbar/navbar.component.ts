import { Component, OnInit, HostListener, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { CollapsedMenuService } from 'src/app/base/services/collapsed-menu.service';
import { UserService } from 'src/app/base/services/user.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  private toggleButton: any;
  private sidebarVisible: boolean;
  private screenHeight: number;
  private screenWidth: number;

  public isMessageAreaVisible: boolean = false;
  public lastMessage: string = "Incoming message...";

  // Level '1' collapsed menu items
  public items: any[] = [
    // =========
    {
      level: 1,
      routerLink: ["/whoweare"],
      label: "?"
    },
    {
      level: 1,
      routerLink: ["/landing"],
      label: "Home page"
    },
    {
      level: 1,
      routerLink: ["/link1"],
      label: "Link 1",
    },
    {
      level: 1,
      routerLink: ["/demo"],
      label: "Bootstrap components",
    },
    // level 2 menu
    {
      level: 2,
      label: 'altro',
      group: 'submenu'
    },
    {
      level: 2,
      sublevel: 1,
      groupMaster: 'submenu',
      routerLink: ['/link2'],
      label: "Cubic Bezier Explorer",
    },
    {
      level: 2,
      sublevel: 1,
      groupMaster: 'submenu',
      routerLink: ['/link3'],
      label: "Pixi.js demo",
    },
    {
      level: 2,
      sublevel: 1,
      groupMaster: 'submenu',
      routerLink: ['/link4'],
      label: "List example",
    },
    {
      level: 2,
      sublevel: 1,
      groupMaster: 'submenu',
      routerLink: ['/link5'],
      label: "Edit example",
    },
  ];

  constructor(
    private element: ElementRef,
    private collapsedMenuService: CollapsedMenuService,
    public userService: UserService,
    private router: Router
  ) { }

  ngOnInit() {
    const navbar: HTMLElement = this.element.nativeElement;
    this.toggleButton = navbar.getElementsByClassName("navbar-toggler")[0];
    this.getScreenSize();
    /*
    this.userService.currentUser.subscribe((user: User) => {
      this.currentUser = user;
    });
    */

    this.collapsedMenuService.register(this);

    // ADD items to the collapsed menu service
    this.collapsedMenuService.addItems(this.items, 1);
  }

  sidebarOpen() {
    const toggleButton = this.toggleButton;
    const html = document.getElementsByTagName("html")[0];
    // console.log(html);
    // console.log(toggleButton, 'toggle');

    setTimeout(function () {
      toggleButton.classList.add("toggled");
    }, 500);
    html.classList.add("nav-open");

    this.sidebarVisible = true;
  }

  sidebarClose() {
    const html = document.getElementsByTagName("html")[0];
    // console.log(html);
    this.toggleButton.classList.remove("toggled");
    this.sidebarVisible = false;
    html.classList.remove("nav-open");
  }

  sidebarToggle() {
    // const toggleButton = this.toggleButton;
    // const body = document.getElementsByTagName('body')[0];
    if (this.sidebarVisible === false) {
      this.sidebarOpen();
    } else {
      this.sidebarClose();
    }
  }

  @HostListener("window:resize", ["$event"])
  getScreenSize(event?): void {
    this.screenHeight = window.innerHeight;
    this.screenWidth = window.innerWidth;
    console.log(this.screenHeight, this.screenWidth);
  }

  isLargeSize(): boolean {
    return this.screenWidth > 991;
  }

  counter(i: number) {
    return new Array(i);
  }

  logout(): void {
    this.userService.logout();
    this.router.navigateByUrl("/home");
  }
}
