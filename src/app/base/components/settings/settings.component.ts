/// <reference types="@types/googlemaps" />

import { Component, Input, OnInit, Output, EventEmitter, ViewChild, ElementRef, NgZone} from "@angular/core";
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { Router } from "@angular/router";
import { MapsAPILoader} from '@agm/core';
import { NgxUiLoaderService } from "ngx-ui-loader";
import { isEON } from "src/app/functions/miscellaneous.functions";
import { HttpClient } from "@angular/common/http";
import { ConfigurationProvider } from 'src/app/classes/ConfigurationProvider';
import { User } from 'src/app/interfaces/user.model';
import { AlertModalContentComponent } from "src/app/library/components/alert-modal-content/alert-modal-content.component";
import { ApplicationService } from "../../services/application.service";
import { UserService } from "../../services/user.service";

declare var google;

// confirmation modal
// ==================
@Component({
  selector: "app-modal-content",
  templateUrl: "./confirmation.component.html",
  styleUrls: ["./confirmation.component.scss"],
})
export class ConfirmationModalContent {
  @Input() email;
  @Input() mobile;
  @Input() waitingForEmailConformation = false;
  @Input() waitingForMobileConformation = false;
  confirmationCode: string;

  @Output() change: EventEmitter<string> = new EventEmitter<string>();

  constructor(public bsModalRef: BsModalRef) {}

  fireEvent(evt) {
    this.change.emit(this.confirmationCode);
  }
}

@Component({
  selector: "app-settings",
  templateUrl: "./settings.component.html",
  styleUrls: ["./settings.component.scss"],
})
export class SettingsComponent implements OnInit {
  // local copy of actual user data
  // ------------------------------
  public currentUser: User;
  // ------------------------------

  // Map parameters
  // ==============
  defaultCenter: any = {lat: 40.350, lng: 18.175};
  latitude: number;
  longitude: number;
  zoom: number = 12;
  address: string;
  private geoCoder;
  @ViewChild('search', { static: true })
  public searchElementRef: ElementRef;

  public credits = {
    requestText: "",
  };

  public repassword: string = "";
  public emailcode: string = "";
  public mobilecode: string = "";

  validPassword: boolean = false;
  validPasswords: boolean = false;
  showPassword = false;
  notAValidPasswordMsg: string = undefined;

  public applicationRoles: any;

  @ViewChild("emailCodeInput", { static: true })
  private emailCodeInput: ElementRef<HTMLInputElement>;

  @ViewChild("mobileCodeInput", { static: true })
  private mobileCodeInput: ElementRef<HTMLInputElement>;

  // Environmental properties
  // ========================
  productionEnvi: boolean;

  constructor(
    private applicationService: ApplicationService,
    private configurationProvider: ConfigurationProvider,
    private userService: UserService,
    private http: HttpClient,
    private modalService: BsModalService,
    private ngxService: NgxUiLoaderService,
    private mapsAPILoader: MapsAPILoader,
    private ngZone: NgZone,
    private router: Router
  ) {
    // set local productionEnvi flag
    this.productionEnvi = configurationProvider.isProductionEnvironment();
  }

  ngOnInit(): void {
    // synchronization of the local copy of user data
    this.userService.currentUser.subscribe((user: User) => {
      this.currentUser = user;
    });

    this.applicationService.getApplicationRoles().then((res) => {
      if (res) {
        this.applicationRoles = res;
      }
    });

    // MAP INITIALIZATION
    // ==================
    this.mapsAPILoader.load().then(() => {
      this.setCurrentLocation();
      this.geoCoder = new google.maps.Geocoder;

      if(this.userService.isUserInOneOfRoles("admin, root")) { // PROVVISORIO

        let autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement);
        autocomplete.addListener("place_changed", () => {
            this.ngZone.run(() => {
                //get the place result
                let place: google.maps.places.PlaceResult = autocomplete.getPlace();
    
                //verify result
                if (place.geometry === undefined || place.geometry === null) {
                  return;
                }
    
                //set latitude, longitude and zoom
                this.latitude = place.geometry.location.lat();
                this.longitude = place.geometry.location.lng();
                this.zoom = 12;

                //change back-end location 
                this.changeCurrentLocation(this.latitude, this.longitude);
            });
        });
      }
    });
    // SET CURRENT LOCATION
    this.setCurrentLocation();
    // === END MAP INITIALIZATION
  }

  isUserAdmin(): boolean {
    return this.userService.isUserInOneOfRoles("admin, root");
  }

  isUserTester(): boolean {
    return this.userService.isUserInOneOfRoles("tester");
  }

  getRequestableRoles(): any {
    let requestableRoles: any = [];

    loop: for (let role of this.applicationRoles) {
      for (let currentRole of this.currentUser.roles)
        if (role.type == 0 || currentRole.name === role.name) continue loop;
      requestableRoles.push(role);
    }

    return requestableRoles;
  }

  update(): void {
    this.ngxService.start();
    this.userService
      .update(this.currentUser)
      .then((res) => {
        this.ngxService.stop();
        if (res) {
          this.alertModal("Profilo utente", "I dati sono stati aggiornati.");
        }
        this.repassword = "";
        this.userService.relogin();
      })
      .catch((err) => {
        this.ngxService.stop();
        console.log("Error logging user: " + err);
      });
  }

  updateInfo(): void {
    let user = this.userService.getEmptyUser();
    user.uuid = this.currentUser.uuid;
    user.info = this.currentUser.info;

    this.userService
      .update(user)
      .then((res) => {
        if (res) {
          this.alertModal("Profilo utente", "I dati sono stati aggiornati.");
        }
        this.repassword = "";
        this.userService.relogin();
      })
      .catch((err) => {});
  }

  isTrustedEmail(): boolean {
    return this.currentUser.isTrustedEmail();
  }

  isTrustedMobile(): boolean {
    return this.currentUser.isTrustedMobile();
  }

  getFullName() {
    return (
      (this.currentUser.firstName ? this.currentUser.firstName : "[nome]") +
      " " +
      (this.currentUser.lastName ? this.currentUser.lastName : "[cognome]")
    );
  }

  getEMail() {
    return this.currentUser.email ? this.currentUser.email : "[e-mail]";
  }

  getPictureUrl() {
    return this.currentUser.picture?'https://downloads.salentopoli.it/users/'+this.currentUser.picture:'./assets/images/no-user-picture.png';
  }

  // credits handling
  // ================
  sendCreditRequest() {}

  // passwords handling
  // ==================
  passwordChanged() {
    this.validPassword = this.isValidPassword();
    this.passwordsChanged();
  }

  passwordsChanged() {
    this.validPasswords =
      this.validPassword && this.currentUser.password == this.repassword;
  }

  changeShowPassword(): void {
    this.showPassword = !this.showPassword;
  }

  isValidPassword(): boolean {
    let response: any = this.userService.isValidPassword(
      this.currentUser.password
    );
    this.notAValidPasswordMsg = response.message;

    return response.isvalid;
  }
  // === end passwords handling

  alertModal(title: string, text: string): void {
    const modalRef = this.modalService.show(AlertModalContentComponent, { animated: true, keyboard: true, backdrop: true, ignoreBackdropClick: false });
    modalRef.content.title = title;
    modalRef.content.text = text;
  }

  sendEmailValidationCode() {
    if (this.isTrustedEmail()) return;

    // Send a confirmation code to email address
    this.userService
      .sendEmailValidationCode(this.currentUser.email)
      .then((res) => {
        this.openEmailModal();
      })
      .catch((err) => {});
  }

  sendMobileValidationCode() {
    if (this.isTrustedMobile()) return;

    // Send a confirmation code to mobile via SMS
    this.userService
      .sendMobileValidationCode(this.currentUser.mobile)
      .then((res) => {
        this.openMobileModal();
      })
      .catch((err) => {});
  }

  changedOptions(optionUUID: string, selected: boolean) {
    this.userService.setOption(optionUUID, selected);
  }

  // MODAL
  // =====
  openEmailModal() {
    const modalRef = this.modalService.show(ConfirmationModalContent);
    /*
    modalRef.result.then(
      (result) => {},
      (reason) => {}
    );
    */
    modalRef.content.email = this.currentUser.email;
    modalRef.content.waitingForEmailConformation = true;

    modalRef.content.change.subscribe((code) => {
      // Verify the confirmation code sent to e-mail address
      this.userService
        .validateEMail(code)
        .then((confirmed) => {
          if (confirmed) {
            this.userService.relogin();
            this.modalService.hide();
          }
        })
        .catch((err) => {});
    });
  }

  openMobileModal() {
    const modalRef = this.modalService.show(ConfirmationModalContent);
    /*
    modalRef.result.then(
      (result) => {},
      (reason) => {}
    );
    */
    modalRef.content.mobile = this.currentUser.mobile;
    modalRef.content.waitingForMobileConformation = true;

    modalRef.content.change.subscribe((code) => {
      // Verify the confirmation code sent to the mobile via SMS
      this.userService
        .validateMobile(code)
        .then((confirmed) => {
          if (confirmed) {
            this.userService.relogin();
            this.modalService.hide();
          }
        })
        .catch((err) => {});
    });
  }

  emailCodeChanged(emailCodeInput: HTMLInputElement) {
    emailCodeInput.classList.remove("h-error-input-value");
  }

  validateEmail(): void {
    if (this.emailcode === "") {
      this.alertModal(
        "Verifica del codice di controllo",
        'Se si dispone di un codice inserirlo nel campo "Codice" prima di digitare il pulsante.' +
          'Altrimenti generare un nuovo codice cliccando sul simbolo a sinistra del campo "E-mail" e seguire la procedura.'
      );
    }

    // Verify the confirmation code sent to e-mail address
    this.userService
      .validateEMail(this.emailcode)
      .then((confirmed) => {
        if (confirmed) this.userService.relogin();
        else
          this.emailCodeInput.nativeElement.classList.add(
            "h-error-input-value"
          );
      })
      .catch((err) => {});
  }

  mobileCodeChanged(mobileCodeInput: HTMLInputElement) {
    mobileCodeInput.classList.remove("h-error-input-value");
  }

  validateMobile(): void {
    if (this.mobilecode === "") {
      this.alertModal(
        "Verifica del codice di controllo",
        'Se si dispone di un codice inserirlo nel campo "Codice" prima di digitare il pulsante.' +
          'Altrimenti generare un nuovo codice cliccando sul simbolo a sinistra del campo "Mobile" e seguire la procedura.'
      );
    }

    // Verify the confirmation code sent to the mobile device via SMS
    this.userService
      .validateMobile(this.mobilecode)
      .then((confirmed) => {
        if (confirmed) this.userService.relogin();
        else
          this.mobileCodeInput.nativeElement.classList.add(
            "h-error-input-value"
          );
      })
      .catch((err) => {});
  }

  isVisible(type: string) {
    switch (type) {
      case "update":
        return (
          (!isEON(this.currentUser.username) || !isEON(this.currentUser.email) || !isEON(this.currentUser.mobile)) 
          &&
          (isEON(this.currentUser.password) || this.validPasswords)
        );
    }
  }

  // MAP functions
  // =============
  private setCurrentLocation() {
    if(this.currentUser.extra.position) {
      this.latitude = this.currentUser.extra.residence.address.position.coordinates.lat;
      this.longitude = this.currentUser.extra.residence.address.position.coordinates.lng;
    }
    else {
      this.latitude = this.defaultCenter.lat;
      this.longitude = this.defaultCenter.lng;
    }
  }

  private changeCurrentLocation(latitude: number, longitude: number) {
    this.latitude = latitude;
    this.longitude = longitude;

    this.currentUser.extra.residence.address.position = {};
    this.currentUser.extra.residence.address.position.coordinates = {};
    this.currentUser.extra.residence.address.position.coordinates.lat = this.latitude;
    this.currentUser.extra.residence.address.position.coordinates.lng = this.longitude;
  }

  markerDragEnd($event: google.maps.MouseEvent) {
    console.log($event);
    this.latitude = $event.latLng.lat();
    this.longitude = $event.latLng.lng();
    this.currentUser.extra.residence.address.position = {};
    this.currentUser.extra.residence.address.position.coordinates = {};
    this.currentUser.extra.residence.address.position.coordinates.lat = this.latitude;
    this.currentUser.extra.residence.address.position.coordinates.lng = this.longitude;

    this.getAddress(this.latitude, this.longitude);
  }

  getAddress(latitude, longitude) {
    this.geoCoder.geocode({ 'location': { lat: latitude, lng: longitude } }, (results, status) => {
      console.log(results);
      console.log(status);
      if (status === 'OK') {
        if (results[0]) {
          this.zoom = 12;
          this.address = results[0].formatted_address;
        } else {
          window.alert('No results found');
        }
      } else {
        window.alert('Geocoder failed due to: ' + status);
      }
    });
  }

  userDeletion(): void {
    const modalRef = this.modalService.show(AlertModalContentComponent);
    modalRef.content.title = "Cancellazione dati utente";
    modalRef.content.confirmButtonText = "CONFERMA della cancellazione";
    modalRef.content.cancelButtonText = "Annullamento";

    // get text from assets
    this.http
    .get("./assets/html/user-deletion.html", { responseType: "text" })
    .subscribe(
      (data) => {
        modalRef.content.text = data;
      },
      (err) => {
        modalRef.content.text = err;
      }
    );

    modalRef.content.confirm.subscribe((confirm) => {
      if(confirm) {
        // ANONYMIZE USER
        this.ngxService.start();
        this.userService.anonymize()
          .then(
            (res) => {
              this.ngxService.stop();
              this.userService.logout();
              this.router.navigateByUrl("/home");
            },
            (err) => {
              this.ngxService.stop();
              alert('error while anonymizing');
            })
      }
    });
  }
}

