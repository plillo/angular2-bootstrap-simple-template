import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/interfaces/user.model';
import { UserService } from '../../services/user.service';

@Component({
    selector: 'app-profile',
    templateUrl: './profile.component.html',
    styleUrls: ['./profile.component.scss']
})

export class ProfileComponent implements OnInit {
    public user: User;

    constructor(private userService: UserService) { 
        this.user = userService.getUser();
      }

    ngOnInit() {}

    getFullName() {
        return (this.user.firstName?this.user.firstName:'[nome]')+' '+(this.user.lastName?this.user.lastName:'[cognome]');
    }

getEMail() {
    return (this.user.email?this.user.email:'[e-mail]');
}

getPictureUrl() {
    return this.user.picture?'https://downloads.salentopoli.it/users/'+this.user.picture:'./assets/images/no-user-picture.png';
}

deletePicture() {
    this.userService.deletePicture('picture')
        .then((res) => {
            this.user = this.userService.getUser();
        })
        .catch((err) => {
        });
}
}
