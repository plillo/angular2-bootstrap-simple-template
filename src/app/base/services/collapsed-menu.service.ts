import { Injectable } from '@angular/core';
import { NavbarComponent } from '../components/layout/navbar/navbar.component';
import { UserService } from './user.service';

@Injectable({
  providedIn: 'root'
})
export class CollapsedMenuService {
  public labels: string[] = [];
  public items: any[] = [];
  navbarComponent: NavbarComponent;

  constructor(private userService: UserService) { }

  register(navbar: NavbarComponent) {
    this.navbarComponent = navbar;
  }

  sidebarToggle() {
    this.navbarComponent.sidebarToggle();
  }  

  addItems(items: any[], level: number, label?: string): void {
    this.removeItems(level);
    if(label)
      this.labels[level] = label;
    else
      this.labels.splice(level, 1);

    for(let item of items) {
        item.level = level;
        item.visibility = item.groupMaster? false : true;

        this.items.push(item);
    }
  }

  removeItems(level: number): void {
    this.items = this.items.filter(function(value, index, arr){ 
      return value.level != level;
    });
  }

  getItems(level: number):any[]  {
    let items: any[] = [];
    for(let item of this.items) {
      if(level==undefined || item.level==level) {
        if(!item.roles || this.userService.isUserInOneOfRoles(item.roles)) 
          items.push(item);
      }
    }
    
    return items;
  }

  toggleGroup(groupMaster: string): void {
    for(let item of this.items) {
      if(item.groupMaster==groupMaster)
        item.visibility = !item.visibility;
    }
  }

  getLabel(level: number): string {
    return this.labels[level];
  }
}
