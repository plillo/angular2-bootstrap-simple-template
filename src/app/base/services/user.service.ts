import { Injectable } from "@angular/core";
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Observable, Subject, BehaviorSubject } from 'rxjs';
import { BackendService } from "./backend.service";
import { JwtService } from "./jwt.service";
import { LocalStorageService } from 'angular-2-local-storage';
import { ApplicationService } from "./application.service";
import { User } from "src/app/library/interfaces/user.model";

const appSettings: any = {};

@Injectable({
  providedIn: 'root'
})
export class UserService {
    private token: string;
    private locale: string = 'it-IT';
    private user: User;
    currentUser: Subject<User> = new BehaviorSubject<User>(null);

    private loading : Boolean = false;

    // constructor
    // ===========
    constructor(
        private http: HttpClient, 
        private jwtService: JwtService,
        private backendService: BackendService,
        private applicationService: ApplicationService,
        private localStorage: LocalStorageService){
    }

    public setCurrentUser(newUser: User): void {
        this.currentUser.next(newUser);
    }

    getEmptyUser():User {
        return new User('',undefined,undefined,undefined,undefined,undefined,undefined,undefined,undefined,undefined,undefined,undefined,undefined);
    }
    
    // get & set 'token'
    getToken(): string {
        return this.token;
    }

    setToken(token: string): void {
        this.token = token;
    }

    // get & set 'locale'
    getLocale(): string {
        return this.locale;
    }

    setLocale(locale: string): void {
        this.locale = locale;
    }

    // <<getUser>> function
    // --------------------
    getUser(): User {
        return this.user;
    }

    // <<setUser>> function
    // --------------------
    setUser(user: User): void {
        this.user = user;

        this.setCurrentUser(user);
    }

    // <<resetUser>> function
    // ----------------------
    resetUser() {
        this.user = undefined;

        this.setCurrentUser(undefined);
    };

    // <<login>> function
    // ------------------
    login(identificator: string, password: string): Promise<string> {
        // set params: injection of 'appcode'
        const params = new HttpParams().set('identificator', identificator).set('password', password).set('appcode', this.applicationService.getCode());

        return new Promise<string>((resolve, reject) => {
            this.http.get(this.backendService.getAuthBackend() + '/core/1.0/users/login', {params})
            .subscribe(
                (data) => {
                    //appSettings.setString("token", data['token']);
                    // Set the new "token" on local-storage
                    this.localStorage.set("token", data['token']);

                    this.setToken(data['token']);

                    console.log("Setted user-token in app settings: "+data['token']);

                    let decodedToken = this.jwtService.decodeToken(data['token']);
                    this.setUserByDecodedToken(decodedToken);

                    /*
                    // if changed update PUSH token (mobile only)
                    // ==========================================
                    let _pushToken = appSettings.getString("pushToken");
                    if(_pushToken && _pushToken!= decodedToken['pushToken']){
                        this.pushTokenUpdate(_pushToken);
                    }
                    */

                    console.log(JSON.stringify( this.getUser()));
    
                    // FIRE LoggedUserEvent
                    // this.loggedUserEvent.fire(`User logged`);
                    // console.log("User logged event fired");

                    resolve("logged");
                }, 
                (error) => {
                    console.log("Error while logging user", error);
                    reject("unlogged");
                }
            );
        });
    }

    // <<logout>> function
    // -------------------
    logout() {
        this.setUser(null);
        this.setToken(null);

        //appSettings.remove("token");
        this.localStorage.remove("token");

        // FIRE UnloggedUserEvent
        // this.unloggedUserEvent.fire(`User unlogged`);
        console.log("User unlogged");
    }

      // <<update>> function
    // -------------------
    update(user: object) {
        user['uuid'] = this.getUser().uuid;

        const headers = new HttpHeaders().set("Content-Type", "application/json");

        return new Promise<string>((resolve, reject) => {
            // PUT [base]/core/1.0/users/[user-uuid]
            // .....................................
            this.http.put(this.backendService.getAuthBackend() + '/core/1.0/users/'+this.getUser().uuid, user, {headers})
            .subscribe(
                (data) => {
                    if(data['status']=="2002") {
                        //appSettings.setString("token", data['token']);
                        // Set the new "token" on local-storage
                        this.localStorage.set("token", data['token']);

                        console.log("Setted user-token in app settings: "+data['token']);
    
                        let decodedToken = this.jwtService.decodeToken(data['token']);
                        this.setUserByDecodedToken(decodedToken);
                        this.setToken(data['token']);

                        console.log("Updated profile: ", JSON.stringify( this.getUser()));

                        resolve("updated");
                    }
                    else {
                        console.log("Updated profile: ", JSON.stringify( data));
                        resolve("unupdated");
                    }
                }, 
                (error) => {
                    console.log("Error while updating user", error);
                    reject("unupdated");
                }
            );
        });
    }

    // <<signin>> function
    // -------------------
    signin(identificator: string, password: string, options?: any[]): Promise<string> {
        const headers = new HttpHeaders().set("Content-Type", "application/json");
        var user = {
            'identificator': identificator, 
            'password': password,
            'appcode': this.applicationService.getCode()
        };
        if(options)
            user['options'] = options;

        return new Promise<string>((resolve, reject) => {
            // PUT [base]/core/1.0/users/[user-uuid]
            // .....................................
            console.log("Signing-in user...");
            this.http.post(this.backendService.getAuthBackend() + '/core/1.0/users/login', user, {headers})
            .subscribe(
                (data) => {
                    this.localStorage.set("token", data['token']);

                    this.setToken(data['token']);

                    console.log("Setted user-token in app settings: "+data['token']);

                    let decodedToken = this.jwtService.decodeToken(data['token']);
                    this.setUserByDecodedToken(decodedToken);

                    /*
                    // if changed update PUSH token (mobile only)
                    // ==========================================
                    let _pushToken = appSettings.getString("pushToken");
                    if(_pushToken && _pushToken!= decodedToken['pushToken']){
                        this.pushTokenUpdate(_pushToken);
                    }
                    */

                    console.log(JSON.stringify( this.getUser()));
    
                    // FIRE LoggedUserEvent
                    // this.loggedUserEvent.fire(`User logged`);
                    // console.log("User logged event fired");

                    console.log("Signed-in user: ", data);
                    resolve("signed");
                }, 
                (error) => {
                    console.log("Error while signing-in user", error);
                    reject("unsigned");
                }
            );
        });
    }

    // <<sendEmailValidationCode>> function
    // -------------------------------
    sendEmailValidationCode(email: string) {
        console.log("sending e-mail address validation code...");
        return new Promise<string>((resolve, reject) => {
            this.http.put(this.backendService.getAuthBackend() + '/core/1.0/users/' + this.user.uuid +'/trustemail/' + email, {})
            .subscribe(
                (data) => {
                    console.log("sent e-mail address validation code: ", data);
                    resolve("ok")
                }, 
                (error) => {
                    console.log("Error while sending e-mail address validation code: ", error);
                    reject("ko");
                }
            );
        });
    }

    // <<validateEMail>> function
    // --------------------------
    validateEMail(code: string) {
        console.log("Validating email...");
        return new Promise<boolean>((resolve, reject) => {
            this.http.put(this.backendService.getAuthBackend() + '/core/1.0/users/' + this.user.uuid +'/email-confirmation/' + code, {})
            .subscribe(
                (data) => {
                    console.log("validation response: ", data);
                    resolve(data['emailConfirmed']);
                }, 
                (error) => {
                    console.log("Error while validating e-mail: ", error);
                    reject(false);
                }
            );
        });
    }

    // <<sendValidationCode>> function
    // -------------------------------
    sendMobileValidationCode(mobile: string) {
        console.log("sending e-mail address validation code...");
        return new Promise<string>((resolve, reject) => {
            this.http.put(this.backendService.getAuthBackend() + '/core/1.0/users/' + this.user.uuid +'/trustmobile/' + mobile, {})
            .subscribe(
                (data) => {
                    console.log("sent mobile validation code: ", data);
                    resolve("ok")
                }, 
                (error) => {
                    console.log("Error while sending mobile validation code: ", error);
                    reject("ko");
                }
            );
        });
    }

    // <<validateEMail>> function
    // --------------------------
    validateMobile(code: string) {
        console.log("Validating mobile...");
        return new Promise<boolean>((resolve, reject) => {
            this.http.put(this.backendService.getAuthBackend() + '/core/1.0/users/' + this.user.uuid +'/mobile-confirmation/' + code, {})
            .subscribe(
                (data) => {
                    console.log("validation response: ", data);
                    resolve(data['mobileConfirmed']);
                }, 
                (error) => {
                    console.log("Error while validating mobile: ", error);
                    reject(false);
                }
            );
        });
    }

    // <<pushTokenUpdate>> function
    // ----------------------------
    pushTokenUpdate(pushToken) {
        console.log("push-token updating: " + pushToken);

        return new Promise<string>((resolve, reject) => {
            this.http.put(this.backendService.getAuthBackend() + '/core/1.0/users/pushtoken?push-token='+ pushToken, {})
            .subscribe(
                (data) => {
                    console.log("Updated user push-token: ", data);
                    resolve("updated push-token")
                }, 
                (error) => {
                    console.log("Error while updating user push-token: ", error);
                    reject("unupdated push-token");
                }
            );
        });
    }

    // <<setUserFromAppSettings>> function
    // -----------------------------------
    setUserFromLocalStorage(): void {
        //let token = appSettings.getString("token");
        let token: string = this.localStorage.get("token");
        if(token) {
            let decodedToken = this.jwtService.decodeToken(token);
            this.setUserByDecodedToken(decodedToken);
            this.setToken(token);

            console.log("Restored user: " + this.getUser().uuid);
        }
        else {
            this.user = undefined;
        }
    }

    // <<isLoggedUser>> function
    // -------------------------
    isUserLogged(): boolean {
        if(this.user == undefined)
            this.setUserFromLocalStorage();

        return this.user !== undefined;
    };
    
    // <<isUserTrusted>> function
    // -------------------------
    isUserTrusted(): boolean {
        if(this.user == undefined)
            return false;

        return this.user._isTrustedEmail || this.user._isTrustedMobile;
    };
    
    // <<isUserInRole>> function
    // -------------------------
    isUserInRole(role: string): boolean {
        if (this.user == undefined || this.user.roles == undefined)
            return false;

        for(let k=0;k<this.user.roles.length;k++)
            if(this.user.roles[k].name==role)
                return  this.user.roles[k].isActive;   
    };

    // <<isUserInAllRoles>> function
    // input parameter example: 'admin,!root,!op'
    // output: true if all rules are matched
    // ------------------------------------------
    isUserInAllRoles(roles: string|string[]): boolean {
        if(typeof roles === 'string')
            roles = roles.split(',');

        for (var k in roles) {
            var role = roles[k].trim();
            if (role.length > 0)
                if (role.charAt(0) == '!') {
                    if (this.isUserInRole(role.substr(1)))
                        return false;
                }
                else if (!this.isUserInRole(role))
                    return false;
        }

        return true;
    };

    // <<isUserInOneOfRoles>> function
    // input parameter example: 'admin,!root,!op'
    // output: true if at least one of rules is matched
    // ------------------------------------------------
    isUserInOneOfRoles(roles: string|string[]): boolean {
        if(typeof roles === 'string')
        roles = roles.split(',');

        for (var k in roles) {
            var role = roles[k].trim();
            if (role.length > 0)
                if (role.charAt(0) == '!') {
                    if (!this.isUserInRole(role.substr(1)))
                        return true;
                }
                else if (this.isUserInRole(role))
                    return true;
        }

        return false;
    };

    // <<getUserAttributeValue>> function
    // ----------------------------------
    getUserAttributeValue(uuid: string): any {
        for (var k = 0; k < this.user.attributeValues.length; k++) {
            if (this.user.attributeValues[k].uuid == uuid)
                return this.user.attributeValues[k].value;
        }

        return undefined;
    };

      // <<trustEMail>> function
    // -----------------------
    trustEMail(): Promise<string> {
        if(!this.isUserLogged)
            return new Promise<string>((resolve, reject) => {reject('unlogged')})

        return new Promise<string>((resolve, reject) => {
            this.http.get(this.backendService.getAuthBackend() + '/core/1.0/users/' + this.user.uuid, {})
            .subscribe(
                (data) => {
                    resolve("sent")
                }, 
                (error) => {
                    reject("unsent");
                }
            );
        });
    };

    validateIdentificator(identificator: string) {
        this.loading = true;

        const params = new HttpParams().set('value', identificator).set('appcode', this.applicationService.getCode());
        return new Promise<Object>((resolve, reject) => {
            let s = this.backendService.getAuthBackend() + '/core/1.0/users/validateIdentificator';
            this.http.get(this.backendService.getAuthBackend() + '/core/1.0/users/validateIdentificator', {params})
            .subscribe(
                (data) => {
                    resolve(data)
                }, 
                (error) => {
                    reject(error);
                }
            );

            console.log(this.backendService.getAuthBackend() + '/core/1.0/users/validateIdentificator');
        });
    }

       // <<forgottenPassword>> function
    // ------------------------------
    forgottenPassword(identificator: string): Promise<string> {
        const params = new HttpParams().set('identificator', identificator);

        return new Promise<string>((resolve, reject) => {
            this.http.get(this.backendService.getAuthBackend() + '/core/1.0/users/forgottenPassword', {params})
            .subscribe(
                (data) => {
                    resolve("sent")
                }, 
                (error) => {
                    reject("unsent");
                }
            );
        });
    };

    // <<relogin>> function
    // ------------------
    relogin():Promise<string> {
        return new Promise<string>((resolve, reject) => {
            this.http.get(this.backendService.getAuthBackend() + '/core/1.0/users/relogin', {})
            .subscribe(
                (data) => {
                    //appSettings.setString("token", data['token']);
                    // Set the new "token" on local-storage
                    this.localStorage.set("token", data['token']);

                    console.log("User re-logged, setted user-token in app settings: "+data['token']);

                    this.setToken(data['token']);
                    this.setUserByDecodedToken(this.jwtService.decodeToken(data['token']));
                    console.log(JSON.stringify( this.getUser()));
    
                    // FIRE ReloggedUserEvent
                    // this.loggedUserEvent.fire(`User logged`);
                    // console.log("User logged event fired");

                    resolve("relogged")
                }, 
                (error) => {
                    console.log("Error while relogging user", error);
                    reject("unrelogged");
                }
            );
        });
    }

    getOptions(): Promise<Object> {
        const params = new HttpParams().set('appcode', this.applicationService.getCode());
        return new Promise<Object>((resolve, reject) => {
            this.http.get(this.backendService.getAuthBackend() + '/core/1.0/users/options', {params})
            .subscribe(
                (data) => {
                    resolve(data)
                }, 
                (error) => {
                    reject(error);
                }
            );
        });
    }

    setPicture(uuid:string) {
        this.user.picture = uuid;
    }

    deletePicture(fieldName: string):Promise<string> {
        return new Promise<string>((resolve, reject) => {
            this.http.put(this.backendService.getAuthBackend() + '/core/1.0/users/deletePicture', {'fieldName': fieldName})
            .subscribe(
                (data) => {
                    // Set the new "token" on local-storage
                    this.localStorage.set("token", data['token']);

                    console.log("User re-logged, setted user-token in app settings: "+data['token']);

                    this.setToken(data['token']);
                    this.setUserByDecodedToken(this.jwtService.decodeToken(data['token']));
                    console.log(JSON.stringify( this.getUser()));

                    resolve("removed")
                }, 
                (error) => {
                    console.log("Error while relogging user", error);
                    reject("unremoved");
                }
            );
        });
    }

    private setUserByDecodedToken(decodedToken:any):void {
        this.setUser(new User(
            decodedToken['uuid'],
            decodedToken['username'],
            decodedToken['email'],
            decodedToken['mobile'],
            decodedToken['firstName'],
            decodedToken['lastName'],
            "",
            decodedToken['address'],
            decodedToken['fiscalCode'], 
            decodedToken['vatNumber'],
            decodedToken['avatar'],
            decodedToken['picture'],
            decodedToken['info'],
            decodedToken['activeRoles'],
            decodedToken['roles']?JSON.parse(decodedToken['roles']):[],
            decodedToken['options']?JSON.parse(decodedToken['options']):[],
            decodedToken['attributeValues'],
            decodedToken['attributeTypes'],
            decodedToken['isTrustedEmail'],
            decodedToken['isTrustedMobile'],
            decodedToken['hasStripeID']
        ));
    }

    public isValidPassword(password: string):{isvalid:boolean, messages:string[]}{
        let isValid: boolean = true;
        let messages: string[] = [];

        if(!/[0-9]/.test(password)) {
            isValid = false;
            messages.push('almeno una cifra');
        }
        if(!/[a-z]/.test(password)) {
            isValid = false;
            messages.push('una lettera minuscola');
        }
        if(!/[A-Z]/.test(password)) {
            isValid = false;
            messages.push('almeno una lettera maiuscola');
        }
        if(!/(?:(-|;|\.|\:|@|[|])){1,}/.test(password)) {
            isValid = false;
            messages.push('almeno uno dei simboli [- ; . : @]');
        }
        if(password.length < 8) {
            isValid = false;
            messages.push('almeno otto caratteri');
        }

        return {isvalid:isValid, messages:isValid?undefined:messages};
    }

    setOption(optionUUID: string, selected: boolean): Promise<string> {
        return new Promise<string>((resolve, reject) => {
            this.http.patch(this.backendService.getAuthBackend() + '/core/1.0/users/'+ this.user.uuid+'/options/'+optionUUID, {selected:selected})
            .subscribe(
                (data) => {
                },
                (data) => {
                }
            )}
        )  
    }

    anonymize(): Promise<string> {
        return new Promise<string>((resolve, reject) => {
            this.http.patch(this.backendService.getAuthBackend() + '/core/1.0/users/'+ this.user.uuid+'/anonymize', {})
            .subscribe(
                (success) => {
                    resolve("anonymized");
                },
                (error) => {
                    reject("error");
                }
            );
        });
    }

    getFullName() {
        let fullName =
          (this.user.firstName ? this.user.firstName : "") +
          " " +
          (this.user.lastName ? this.user.lastName : "");
        if (fullName == " ") fullName = this.user.email;
    
        return fullName;
    }
    
    private handleError (error: Response | any) {
        console.error(error.message || error);
        return Observable.throw(error.message || error);
    }
}

