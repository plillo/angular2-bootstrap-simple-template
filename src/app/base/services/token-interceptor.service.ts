import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from "rxjs";
import { UserService } from "./user.service";
import { ApplicationService } from "./application.service";
import { BackendService } from './backend.service';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {
  constructor(private userService: UserService, private applicationService: ApplicationService, private backendService: BackendService) {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if(
      request.url.startsWith(this.backendService.getAuthBackend()) 
      || 
      request.url.startsWith(this.backendService.getBlogBackend())
      || 
      request.url.startsWith(this.backendService.getApplicationBackend())
    ) {
      if(this.userService.getToken()) { // with Authorization header
        request = request.clone({
          setHeaders: {
            'Authorization': `Bearer ${this.userService.getToken()}`, // token injection
            'Accept-Language': this.userService.getLocale(), // locale injection
            'Tenant-ID': this.applicationService.getTenantID() // tenantID injection
          }
        });
      }
      else { // without Authorization header
        request = request.clone({
          setHeaders: {
            'Accept-Language': this.userService.getLocale(), // locale injection
            'Tenant-ID': this.applicationService.getTenantID() // tenantID injection
          }
        });
      }
    }
    return next.handle(request);
  }
}