import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BackendService } from '../backend.service';
import { UserService } from '../user.service';

@Injectable({
  providedIn: 'root'
})
export class ContentService {

  constructor(private http: HttpClient, private userService: UserService, private backendService: BackendService) { }

  get(uuid: string) {
    return this.http.get(this.backendService.getAuthBackend() + '/core/1.0/users/' +this.userService.getUser().uuid + '/contents/'+ uuid, {});
  }

  post(data: any) {
    return this.http.post(this.backendService.getAuthBackend() + '/core/1.0/users/' +this.userService.getUser().uuid + '/contents', data);
  }

  put(data: any) {
    return this.http.put(this.backendService.getAuthBackend() + '/core/1.0/users/' +this.userService.getUser().uuid + '/contents', data);
  }

  putByFormData(data: FormData) {
    let url = this.backendService.getAuthBackend() + '/core/1.0/users/' +this.userService.getUser().uuid + '/contents/' + data.get('uuid');
    return this.http.put(url, data);
  }
 
}
