import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BackendService } from './backend.service';
import { ConfigurationProvider } from 'src/app/classes/ConfigurationProvider';

@Injectable({
  providedIn: 'root'
})
export class ApplicationService {
  constructor(private configuration: ConfigurationProvider, private http: HttpClient, private backendService: BackendService) { 
  }

  getCode(): string {
    return this.configuration.getApplicationCode();
  };

  getTenantID(): string {
    return this.configuration.getTenantID();
  };

  // <<getApplicationRoles>> function
  // --------------------------------
  getApplicationRoles():Promise<string> {
    return new Promise<string>((resolve, reject) => {
        this.http.get(this.backendService.getAuthBackend() + '/core/1.0/applications/'+this.getCode()+'/roles', {})
        .subscribe(
            (data) => {
                resolve(data['roles']);
            }, 
            (error) => {
                reject(undefined);
            }
        );
    });
  }
}
