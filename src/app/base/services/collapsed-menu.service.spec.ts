import { TestBed } from '@angular/core/testing';

import { CollapsedMenuService } from './collapsed-menu.service';

describe('CollapsedMenuService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CollapsedMenuService = TestBed.get(CollapsedMenuService);
    expect(service).toBeTruthy();
  });
});
